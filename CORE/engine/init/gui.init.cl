(defpackage :tamias-gui
  (:use :cl)
  (:nicknames :gui)
  (:export state.init-gui))

(in-package :gui)

(defun state.init-gui (state sub-state &optional ui-elements)
  "Placeholder?")
