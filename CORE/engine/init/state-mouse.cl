(in-package :states)

(defun mouse-flags (move? mouse-state)
  (let ((bit-mask 0))
    (incf bit-mask (gethash (if move? :move :nil) modifiers-table))
    (incf bit-mask (gethash (or mouse-state :nil) modifiers-table))
    bit-mask))
    
(defun mouse-input (mouse-button mouse-state state sub-state move?)
  (let ((flags (mouse-flags move? mouse-state)))
    (incf flags (kb-key-masks))
    (loop :for fn :in (get-input :mouse
				 mouse-button flags state sub-state)
	  :do (eval fn))))

(defmacro add-mouse ((mouse-button mouse-state &rest flags) (state sub-state) &body body)
  `(let ((flags 0))
     (incf flags (gethash ,mouse-state modifiers-table))
     (loop :for flag :in ',flags
	   :do (incf flags (gethash flag modifiers-table)))
     (define-input :mouse ((state-symbol ,state) (aux:to-keyword ',sub-state)) (,mouse-button flags) ',body)))
;;     (push ',@body (gethash flags (gethash ,mouse-state (get-state-mouse ,state (aux:to-keyword ',sub-state))))))

