"Sdl2 refers to left super as :left-gui"
(defstruct modifiers
  control
  shift
  meta)
(defvar modifiers (make-modifiers))

(defun alt-t ()
  (modifiers-meta modifiers))
(defmacro meta-t ()
  (alt-t))
(defun ctrl-t ()
  (modifiers-control modifiers))
(defmacro control-t ()
  (ctrl-t))
(defun shift-t ()
  (modifiers-shift modifiers))
"Shift modifier not to be used with the add-key function, but rather with the mouse
Should end up being (add-mouse ... (if (shift-t) x y))"

(defmacro tamias-key-input (t-k)
  `(elt ,t-k 0))
(defmacro tamias-key-state (t-k)
  `(elt ,t-k 1))

(defvar tamias-key-queue nil)

(defmacro push-tamias-key-input (&key key k-state)
  `(aux:push-to-end (vector ,key ,k-state) tamias-key-queue))

;;(defvar tamias-current-key (vector nil nil 0 nil nil))
(push-init (push-tamias-key-input :key nil :k-state nil))
;;#(key key-state frames-held is-held? is-processed?)
(export '(modifiers-control
	  modifiers-shift
	  modifiers-meta
	  modifiers
	  alt-t
	  meta-t
	  ctrl-t
	  control-t
	  shift-t))
