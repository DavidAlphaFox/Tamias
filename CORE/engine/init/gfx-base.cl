(defun set-bg-color (color)
  (if (not (or (listp color) (vectorp color)))
      (tamias:console.add-message "Invalid value passed to set-bg-color")
      (setf tamias:render-clear-color color)))
  
(defun set-clear-color (color)
  (if (not (or (listp color) (vectorp color)))
      (tamias:console.add-message "Invalid value passed to set-clear-color")
      (set-bg-color color)))

(defpackage render
  (:use :cl)
  (:export draw-line
	   line
	   define-buffer
	   create-texture
	   with-rectangle
	   create-rectangle
	   free-rectangle
	   tex-blit
	   box
	   bg-element
	   rectangle
	   to-buffer
	   reset-text-buffer
	   text-buffer
	   ;;render-buffer
	   text-width
	   text-height
	   text-dimensions
	   render-string
	   text
	   font-color
	   image
	   cell
	   animate
	   tile
	   sprite))
(in-package :render)
#|
==============================================================================
                                  GENERAL
==============================================================================
|#
(defun set-buffer (str buffer)
  )
(defmacro to-buffer (str buffer)
  `(setf ,buffer (set-buffer ,str ,buffer)))

(defun draw-line (x y x2 y2 &key (color (vector 255 255 255 255)))
  )

(defmacro line (x y x2 y2 &key (color (vector 255 255 255 255)))
  `(render:draw-line ,x ,y ,x2 ,y2 :color ,color))


(defmacro define-text-buffer (buffer)
  `(progn (defvar ,buffer nil)
	  (push ,buffer tamias:text-buffers)))


(defun create-texture (&key format (access 0) (width 16) (height 16) (color tamias.colors:+white+))
  )

(defmacro with-rectangle (name rect-parameters &body body)
  )
#|
usage: (with-rectangle bat-rect (list (bat-x bat) (bat-y bat) (bat-width bat) (bat-height bat))
(render:rectangle bat-rect))
|#

(defmacro create-rectangle (rect-vals)
  )
#|
usage: (defun some-func ()
          (let ((my-rect (create-rectangle '(x y width height))))
            (render:rectangle my-rect)
            (render:free-rectangle my-rect)))
|#
(defmacro free-rectangle (rect)
  )

(defun image (img &optional (x 0) (y 0) w h)
  )

(defun tex-blit (tex &key src dest color angle center (flip :none))
  )

(defun box (x y w h &optional (color (vector 255 255 255 255)) (filled t))
  ) ;;frees memory taken up by the rect. Not doing this will cause a segfault

(defun bg-element (&optional x y w h color)
  (let ((x (or x 0))
	(y (or y 0))
	(w (or w 32))
	(h (or h 32))
	(color (or color tamias.colors:+chalk-white+)))
    (box x y w h color)))


(defun rectangle (rect &optional (color (vector 255 255 255 255)) filled)
  )


(defun pixel (x y &optional (color (vector 255 255 255 255)))
  )

#|(defun calculate-arc-slope (x1 y1 x2 y2)
  (let ((rise (- y2 y1))
	(run (- x2 x1))
	slope-case)
    (if (< rise 0)
	;;going up
	    ;;going left
	    ;;going right
	;;going down
	    ;;going left
	    ;;going right
	)
    (case slope-case
      (SE );;midpoint C = + (/ run 2) x1 :: + (/ rise 2) y1
      (SW );; C = + (/ run 2) x1 :: + (/ rise 2) y1
      (NW );; C = + (/ run 2) x1 :: + (/ rise 2) y1
      (NE );; C = + (/ run 2) x1 :: + (/ rise 2) y1
      ) ;;NOTE: I may be able to just use C = ... for the midpoint regardless of direction
    )
  )

(defun circle (cx cy r &key filled)
  ;;draw 4 arcs
  (let ((x1 cx)
	(x2 (+ cx r))
	(x3 (- cx r))
	(y1 cy)
	(y2 (- cy r))
	(y3 (+ cy r)))
    ))

|#
#|
In the end, the goal is to get a list of points, and to use that list to draw a circle
Interestingly, because of how I'm getting the "slope", I don't need to reverse the list of the slope
Instead, I can apply that list in the opposite direction
I.e., if a list is going right-down, +X +Y then when it would be going up-left, -Y -X
  |#  
    ;;draw order: x1::y2  x2::y1  x1::y3  x3::y1
    ;;filled uses lines, not-filled goes along a list of points and fills in those points
    ;;ignore points that are past x + r, x -r, y + r and y - r
    ;;or x - r <= x <= x + r and y -r <= y <= y + r
    ;;general idea is that from x1::y2 to x2::y1 would be a "long less-long middle less-middle one" pattern, then reverse that pattern "one ... middle...long"
    ;;Where the x slope goes down, while y slope does not change, so x begins as 6, then 5, 4 and so on. then the pattern that started with x is reversed and is applied to y
    ;;while the x slope "=" 1, just like y.
    ;;so, pseudocode:
    #|
    while x-slope > 0
    loop below x-slope
    do incf x-pixel
       collect pair x-pixel y-pixel
       END
    incf y-pixel
    decf x-slope
    END REP
    |#
    
  
#|

Another methodology for drawing a curved arc:
First get the slope between two points in a rectangle
We'll call these two points, which are the beginning and end of an arc, A and B
After getting the slope of A and B, determine the midpoint, which will be called C
Create a new point D that is a straight line from C, so as to be perpendicular to line AB
Determine the midpoint of CD, E
Determine slope of AE, where the Angle of CAD is 45 degrees
Divide the slope of X of AE by the y slope of AE
This is the amount of X steps required to create a curved arc in the X direction
Upon completion of the X steps, switch to Y steps
Determine an evenly spaced out step amount, where it is something like (4 2 1)
Move 4 in the x direction, rendering a pixel per point
Move 1 in the y direction, render
Move 2 in X, render
Move 1 in y
Move 1 in X
Reverse the list, and switch to Y steps
upon completion of the X and Y steps, a curved line will be drawn

Drawing a circle:
Arc method, but upon completion of Arc 1, reverse X direction
Arc 2, reverse y
Arc 3, reverse X
Arc 4 will be the completed circle

Example:
        A----------------------------D
         \--\  	         	 --/
	  \  -----\   	      --/
	   \	   ---\     -/
            \	       ---E/
	     \	      --/|
	      \	   --/	 |
	       \C-/   	/
                \     	|
		 \    	|
		  \    /
		   \   |
		    \ /
		     \|
                      B
|#

#|
==============================================================================
                              TEXT AND STRINGS
==============================================================================
|#


(defvar font-color (vector 255 255 255 0))

(defmacro reset-text-buffer (buffer)
  )

#|
(defun render-buffer (buffer menu &key color)
  (if color
      (sdl2:set-texture-color-mod buffer (car color) (cadr color) (caddr color)))
  (let ((src (sdl2:make-rect 0
			     0
			     (sdl2:texture-width buffer)
			     (sdl2:texture-height buffer)
			     ))
	(dest (sdl2:make-rect (+ (menu-x menu) 8)
			      (+ (menu-y menu) 8)
			      (- (menu-width menu) 8)
			      (- (menu-height menu) 8)
			      )))
    (sdl2:render-copy tamias:renderer
		      buffer
		      :source-rect src				    
		      :dest-rect dest)
    (sdl2:free-rect src)
    (sdl2:free-rect dest)))

This was for the roguelike I was/am working on
|#

(defun text-width (text)
  (* (length text) (car tamias.string:character-size))
  )
(defun text-height (text)
  (let ((new-line-count (1+ (count #\newline text))))
    (* new-line-count (cadr tamias.string:character-size))))
(defun text-dimensions (text)
  (if (symbolp text)
      (setf text (write-to-string text)))
  (if (stringp text)
      (let ((width (text-width text))
	    (height (text-height text)))
	(list width height))
      (list 0 0)))


(defun render-string (str x y &key width height string-width string-height (rotate 0) (color font-color) anti-alias)
  )

(defun determine-string-width-nlc (str)
  )

(defun text (str x y &key width height (rotate 0) (color font-color) scale origin-is-center)
  )
#|
old text-buffer code
do (setf (values cell-row cell-column) (truncate (char-code (aref string n)) 16))
;;In order to display the text-strings correctly, the code must check for a newline character before n and use that for the divisor,
;;or use the position of #\Newline if there is no newline character before n
(if (find #\NewLine string)
    (case buffer-source
      (array (setf (values mod-y mod-x) (truncate n (1+ (position #\Newline string)))))
      (text (incf mod-x 1)
	    (incf temp-value 1)
	    (if (eq (aref string n) #\NewLine)
		(progn (setf mod-x -1)
		       (incf mod-y 1)
		       (setf temp-value 0))
		(if (and (eq (mod temp-value (/ width (car character-size))) 0)
			 (> mod-x 0))
		    (progn (setf mod-x 0)
			   (incf mod-y 1)
			   (setf temp-value 0))))))
    (if (eq buffer-source 'text)
	(progn (incf mod-x 1)
	       (if (and (eq (mod n (/ width (car character-size))) 0)
			(> mod-x 0))
		   (progn (setf mod-x 0)
			  (incf mod-y 1))))
	(setf (values mod-y mod-x) (truncate n (length string)))))
(if (not (eq (char string n) #\NewLine))
    (let ((color (case (aref string n)
		   (#\w (list 10 10 200))
		   (#\O (list 66 34 10))
		   (#\# (list 126 94 60))
		   (#\. (list 16 60 17))
		   (otherwise (list 255 255 255)))))
      (if (eq buffer-source 'array)
	  (if (not (eq (char string n) #\0))
	      (render-character-to-buffer (list cell-row cell-column)
					  (+ x (* mod-x (car character-size)))
					  (+ y (* mod-y (cadr character-size)))
					  buff
					  :color color))
	  (render-character-to-buffer (list cell-row cell-column)
				      (+ x (* mod-x (car character-size)))
				      (+ y (* mod-y (cadr character-size)))
				      buff
				      :color (list 255 255 255)))
      )))
|#


#|
==============================================================================
                           SPRITES and ANIMATION
==============================================================================
|#

(defpackage :sprite
  (:use :cl)
  (:export make-sheet
	   sheet-file
	   sheet-width
	   sheet-height
	   sheet-cells
	   sheet-rows
	   sheet-columns
	   sheet-surface
	   sheet-texture
	   sheet-cell-timer
	   sheet-cell-size
	   sheet-cell-max-time
	   sheet-current-cell
	   set-sheet-width
	   set-sheet-height
	   set-cells
	   set-sheet-surface
	   set-sheet-texture
	   load-sheet
	   defsheet
	   free-sheet
	   sheets
	   get-sheet))
(in-package :sprite)

(defvar sheets (make-hash-table))
(defvar sheet-images (make-hash-table))

(defstruct sheet
  cell-size
  file
  width
  height
  cells
  rows
  columns
  surface
  texture
  (cell-timer (timer:make :end 20 :reset t))
  (current-cell 0))

#|
;;This is for working with the animation subsystem easier
;;When you do "sprite:defsprite" or "sprite:make" you have to supply an entity-type
;;if the sheet /isn't/ a sheet structure, it'll need to make it at defsprite time
;;So: Sheets are the "technical" part 
;;whereas sprites are the front-end part that are supposed to be easier to manage
;;well maybe
;;I don't know honestly.
;;I feel like it's fine as it is, but I'm not 100% yet
;;
(defstruct sprite 
  (sheet (make-sheet))
  (cell-timer (timer:make :end 20 :reset t))
  (current-cell 0))
|#

#|
(defmacro optimize-sheet (var)
  )
|#

(defmacro set-sheet-width (sheet width)
  `(setf (sprite:sheet-width ,sheet) ,width))

(defmacro set-sheet-height (sheet height)
  `(setf (sprite:sheet-height ,sheet) ,height))

(defmacro set-cells (sheet)
  `(let* ((tile-size (sprite:sheet-cell-size ,sheet))
	  (rows (/ (sprite:sheet-height ,sheet) (cadr tile-size)))
	  (columns (/ (sprite:sheet-width ,sheet) (car tile-size)))
	  (cells (loop :for y :below (sprite:sheet-height ,sheet) :by (cadr tile-size)
		       :append (loop :for x :below (sprite:sheet-width ,sheet) :by (car tile-size)
				     :collect (list x y)))))
     (setf (sprite:sheet-rows ,sheet) rows
	   (sprite:sheet-columns ,sheet) columns
	   (sprite:sheet-cells ,sheet) cells)
     ))

(defmacro set-sheet-surface (sheet surface)
  `(setf (sprite:sheet-surface ,sheet) ,surface))

(defun load-sheet (sheet &optional surface?)
#|  `(let* ((filename (sheet-file ,sheet))
	  (surface (tamias:load-image filename)))
     (set-sheet-height ,sheet (sdl2:surface-height surface))
     (set-sheet-width ,sheet (sdl2:surface-width surface))
     (set-cells ,sheet)
     (set-sheet-surface ,sheet surface)
     (optimize-sheet ,sheet))
|#
  )

(defun check-sheet (entity-type)
  (if (not (stringp (gethash entity-type sheet-images)))
      t))
(export 'check-sheet)

(defun get-sheet (entity-type)
  (let ((ret-val (eval (gethash entity-type sheets))))
    (if (stringp (gethash entity-type sheet-images))
	(setf (gethash entity-type sheet-images) (tamias:load-image (sheet-file ret-val))))
#|
    (eval `(t.aux:add-init (setf (gethash ,entity-type sheet-images)
				 (tamias:load-image (sheet-file ,ret-val)))))
    (eval `(t.aux:add-quit (progn (tamias:free-image (gethash ,entity-type sheet-images))
				  (setf (gethash ,entity-type sheet-images) nil))))
|#
    (setf (sheet-texture ret-val) (gethash entity-type sheet-images))
    (load-sheet ret-val)
    ret-val))

(defvar default-frame-time 6)

(defun defsheet (entity-type file cell-size &optional (animate? t) (frame-time default-frame-time))
  (if (not (listp cell-size))
      (setf cell-size (list cell-size cell-size)))
  (setf (gethash entity-type sheets) `(make-sheet :file ,file
						  :cell-size ',cell-size
						  :cell-timer (timer:make :end ,frame-time
									  :reset t)))
  (setf (gethash entity-type sheet-images) file))

(defun free-sheet (entity-type sheet)
  )

#|
usage:
(defsheet :bat "game/gfx/bat-sheet.png" '(cell-width cell-height))
then (entity-sheet bat) for the bat's sheet
also, the idea is that "entity" is a generalized structure in which all of your entity structs
are based on, mainly to help in reducing necessary labour time
|#

(in-package :render)

(defun cell (sheet cell x y &key width height color (angle 0) center (flip :none))
  (let* ((cells (sprite:sheet-cells sheet))
	 (tile-width (car (sprite:sheet-cell-size sheet)))
	 (tile-height (cadr (sprite:sheet-cell-size sheet)))

	 (src-rect (tamias:make-rect (nth 0 (nth cell cells))
				     (nth 1 (nth cell cells))
				     tile-width
				     tile-height))
	 (dest-rect (tamias:make-rect x
				      y
				      (if width
					  width
					  tile-width)
				      (if height
					  height
					  tile-height)))
	 (flip (if (or (eq flip :none)
		       (eq flip :horizontal)
		       (eq flip :vertical))
		   flip
		   :none))
	 (center (if center
		     center
		     nil))
	 (angle (if (not (or (integerp angle)
			     (floatp angle)))
		    0
		    angle)))
    (tex-blit (sprite:sheet-texture sheet)
	      :src src-rect :dest dest-rect
	      :color color :angle angle :center center :flip flip)))

(defun tile (sheet cell x y &key width height color (angle 0) center (flip :none))
  (cell sheet cell
	x y
	:width width :height height :color color :angle angle :center center :flip flip))

(defun sprite (sheet x y &key width height color (angle 0) center (flip :none))
  (cell sheet (sprite:sheet-current-cell sheet)
	x y
	:width width :height height :color color :angle angle :center center :flip flip))

(defun animate (sheet x y &key width height (row 1) color (angle 0) center (flip :none))
  (when tamias:animation?
    (timer:increment (sprite:sheet-cell-timer sheet))
    (when (timer:end? (sprite:sheet-cell-timer sheet))
      (incf (sprite:sheet-current-cell sheet) 1))
    (if (>= (sprite:sheet-current-cell sheet) (* row (sprite:sheet-columns sheet)))
	(setf (sprite:sheet-current-cell sheet)
	      (- (* row (sprite:sheet-columns sheet))
		 (sprite:sheet-columns sheet))))
    (cell sheet (sprite:sheet-current-cell sheet)
	  x y
	  :width width :height height :color color :angle angle :center center :flip flip)))

