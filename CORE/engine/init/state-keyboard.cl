(in-package :states)

(defmacro add-any-input ((state sub-state) &rest functions)
  `(loop :for fn :in ',functions
	 :do (aux:push-to-end fn (gethash (aux:to-keyword ',sub-state) (state-any-input ,state)))))
(export 'add-any-input)

(defun tamias-key-handler (key key-state state sub-state &optional ctrl alt shift hyper)
  (let ((mod-state 0))
    (if shift
	(incf mod-state (gethash :shift modifiers-table)))
    (if ctrl
	(incf mod-state (gethash :ctrl modifiers-table)))
    (if alt
	(incf mod-state (gethash :alt modifiers-table)))
    (if hyper
	(incf mod-state (gethash :hyper modifiers-table)))
    (incf mod-state (gethash key-state modifiers-table))
#|    (if (not (gethash key (gethash key-state (get-state-keys state sub-state))))
	(setf (gethash key (gethash key-state (get-state-keys state sub-state)))
	      (make-hash-table)))
|#
    ;;(print-objs key key-state state sub-state mod-state)
    ;;    (format t "~A ~A" (get-state-keys state sub-state) key-state)
    (process-top-level-input :keyboard key mod-state)
    (if (gethash sub-state (state-any-input state))
	(loop :for fn :in (gethash sub-state (state-any-input state))
	      :do (eval fn))
	(loop :for t-func :in (get-input :keyboard
					 key mod-state state sub-state)
	      :do  (eval t-func)))))

(defmacro ctrl-flag (ctrl)
  `(if ,ctrl
       :ctrl
       nil))
(defmacro alt-flag (alt)
  `(if ,alt
       :alt
       nil))
(defmacro shift-flag (shift)
  `(if ,shift
       :shift
       nil))

(defmacro get-kb-key (key (state sub-state key-state mod-state))
  `(gethash ,key (get-state-keys ,state ,sub-state)))


(defmacro def-key (key key-state (state sub-state mod-state) &body body)
  `(define-input :keyboard ((state-symbol ,state) (aux:to-keyword ,sub-state))
     (,key ,key-state ,mod-state)
     ',body))
			     
"mod-state is a pseudo binary encoding of the modifier states"


(defmacro define-key (key key-state (state sub-state &optional shift control alt hyper) &body body)
  `(let ((mod-state 0)
	 (key ,key))
     (if (find-if #'alpha-char-p (write-to-string ,key))
	 (if ,shift
	     (incf mod-state #b1)))
     (if ,control
	 (incf mod-state #b10))
     (if ,alt
	 (incf mod-state #b100))
     (if ,hyper
	 (incf mod-state #b1000))
     (define-input :keyboard ((state-symbol ,state) (aux:to-keyword ,sub-state))
	  (,key ,key-state mod-state)
       ',body)))

(defmacro add-state-kb-key (key key-state (state sub-state &optional shift control alt hyper)
			    &body body)
  `(define-input :keyboard ((state-symbol ,state) (aux:to-keyword ',sub-state))
       (,key ,key-state (when ,shift :shift) (when ,control :ctrl)
	     (when ,alt :alt) (when ,hyper :hyper))
     ',body))
(export 'add-state-kb-key)


#|
Comment below is far out of date, but, I feel it could be useful for posterities sake
Using a modifier key based on a combination of 'modifiers' is better than hash tables
for each modifier

"Add-key will only be for states, considering getting rid of sub-states"
(defmacro add-key (key (state sub-state key-state &key ctrl alt shift) &body body)
  "ultimately it should look like '(:ctrl (:shift (:alt ...))); for the hash-tables, depending
on if ctrl alt and shift are to be held down, if shift and alt are nil, 
then it should be (:ctrl ...) and none should be ...
So, an alist 
:I Not going to let conditionals be remapped for the time being. The amount of infrastructure to get them up and running is...messy"
  `(progn (let ((ctrl (ctrl-flag ,ctrl))
		(alt (alt-flag ,alt))
		(shift (shift-flag ,shift)))
	    (push (list ,key (quote ,@body))
		  (gethash ,sub-state (gethash ,key-state (state-keys ,state))))
	  ;;(push (list ,key (list (list ctrl (list (list alt (list (list shift `(quote ,@body))))))))	
	    )	  
	  (defmethod tamias-key ((key (eql ,key))
				 (key-state (eql ,key-state))
				 (state (eql ,state))
				 (sub-state (eql ,sub-state))
				 (ctrl (eql ,ctrl))
				 (alt (eql ,alt))
				 (shift (eql ,shift)))
	    ,@body)))
|#

#|

Whelp, gotta redo this code now haha

Should work, untested because un needed at this point for personal projects
|#


#|
(defun remap-kb-key (orig target key-state state sub-state &optional (mod-state-1 '(nil nil nil)) (mod-state-2 '(nil nil nil)))
  "Mod-State structured as: (shift ctrl alt), so (nil nil nil), will work"
  (let* ((shift1 (shift-flag (car mod-state-1)))
	 (ctrl1 (ctrl-flag (cadr mod-state-1)))
	 (alt1 (alt-flag (caddr mod-state-1)))
	 (mod-state-orig 0)
	 (key-1 
	   (gethash orig (get-state-keys state sub-state)))
	 (key-func-1 nil)
	 (ctrl2 (ctrl-flag (cadr mod-state-2)))
	 (alt2 (alt-flag (caddr mod-state-2)))
	 (shift2 (shift-flag (car mod-state-2)))
	 (mod-state-target 0)
	 (key-2
	   (gethash target (get-state-keys state sub-state)))
	 (key-func-2 nil))
    (if (not (tamias-input-p key-1))
	(setf (gethash orig (get-state-keys state sub-state)) (make-tamias-kb-input)))
    (if (not (tamias-input-p key-2))
	(setf (gethash target (get-state-keys state sub-state)) (make-tamias-kb-input)))
    (if (find-if #'alpha-char-p (write-to-string orig))
	(if shift1
	    (incf mod-state-orig #b1)))
    (if ctrl1
	(incf mod-state-orig #b10))
    (if alt1
	(incf mod-state-orig #b100))
    (if (find-if #'alpha-char-p (write-to-string target))
	(if shift2
	    (incf mod-state-target #b1)))
    (if ctrl2
	(incf mod-state-target #b10))
    (if alt2
	(incf mod-state-target #b100))
    (setf key-func-1 (gethash mod-state-orig (gethash key-state (tamias-input-states-table key-1)))
	  key-func-2 (gethash mod-state-target (gethash key-state (tamias-input-states-table key-1)))
	  (gethash mod-state-orig (gethash key-state (tamias-input-states-table key-1))) key-func-2
	  (gethash mod-state-target (gethash key-state (tamias-input-states-table key-1))) key-func-1)))


|#
