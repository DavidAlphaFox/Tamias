;;(defvar tamias-mouse-button #('button 'button-state 'moving? timer held?))

(defvar mouse-timer 0)
(defvar *t-mouse* (vector 0 nil nil mouse-timer nil))
(defvar *mb* #(nil :left :middle :right))

(defun mouse-button-check (button)
  (when (and (eq tamias:*mouse-velocity-x* 0)
	     (eq tamias:*mouse-velocity-y* 0))
    (setf (elt *t-mouse* 2) nil))
  (setf (elt *t-mouse* 0) (aref *mb* button)
	(elt *t-mouse* 1) :down
	(elt *t-mouse* 3) mouse-timer
	(elt *t-mouse* 4) (> mouse-timer 2)))

(defun mouse-button-release-check (button)
  (setf mouse-timer 0
	*t-mouse* (vector (aref *mb* button) :up
			  nil mouse-timer nil)))
;;call trap on state change

;;(format t "~A ~A ~A" sdl2-ffi:+sdl-button-left+ sdl2-ffi:+sdl-button-middle+ sdl2-ffi:+sdl-button-right+)

(defun mouse-move (button x y xrel yrel)
  (setf tamias:*mouse-x* x
	tamias:*mouse-y* y
	tamias:*mouse-velocity-x* xrel
	tamias:*mouse-velocity-y* yrel)
  (let ((m-state (if (> button 0) :down :up))
	(m-button button))
    (when (eq m-button 4)
      (setf m-button 3))
    (setf (elt *t-mouse* 0) (aref *mb* m-button)
	  (elt *t-mouse* 1) m-state
	  (elt *t-mouse* 2) t
	  (elt *t-mouse* 3) mouse-timer)))
	  
(defmacro test-mouse-width (x-point point-width)
  `(and (>= (- tamias:*mouse-x* tamias:*cursor-size*) (- ,x-point tamias:*cursor-size*))
	(<= (+ tamias:*mouse-x* tamias:*cursor-size*) (+ ,x-point ,point-width tamias:*cursor-size*))))

(defmacro test-mouse-height (y-point point-height)
  `(and (>= (- tamias:*mouse-y* tamias:*cursor-size*) (- ,y-point tamias:*cursor-size*))
	(<= (+ tamias:*mouse-y* tamias:*cursor-size*) (+ ,y-point ,point-height tamias:*cursor-size*))))

(defmacro test-mouse-x (point-x)
  `(and (<= (- tamias:*mouse-x* tamias:*cursor-size*) ,point-x)
	(>= (+ tamias:*mouse-x* tamias:*cursor-size*) ,point-x)))

(defmacro test-mouse-y (point-y)
  `(and (<= (- tamias:*mouse-y* tamias:*cursor-size*) ,point-y)
	(>= (+ tamias:*mouse-y* tamias:*cursor-size*) ,point-y)))

(defun mouse.test-position (x y &optional (width 0) (height 0) (collision-method 'point))
  (case collision-method
    (point (and (test-mouse-x x) (test-mouse-y y)))
    (box (and (test-mouse-width x width) (test-mouse-height y height)))))

(defun mouse.test-boundary (x y width height)
  (mouse.test-position x y width height 'box))

(defun print-mouse ()
  (format t " X: ~A   Y: ~A   Velocity: ~A ~A
" tamias:*mouse-x* tamias:*mouse-y*
	  tamias:*mouse-velocity-x*
	  tamias:*mouse-velocity-y*))

(defun mouse.where? ()
  (format t " X: ~A   Y: ~A" tamias:*mouse-x* tamias:*mouse-y*))
