#|
(DEFVAR CATCH-ONCE NIL)
(DEFVAR CATCH-VALUES NIL)
(DEFUN CATCH-VALUE (VALUE)
  (IF (NOT CATCH-ONCE)
      (PUSH VALUE CATCH-VALUES)))
(DEFUN CATCH-VALUES (&rest VALUE)
  (PUSH VALUE CATCH-VALUES))


(defmacro push-to-end (obj lst)
  `(if ,lst
       (push ,obj (cdr (last ,lst)))
       (push ,obj ,lst)))
;;Adapted from
;;https://stackoverflow.com/questions/13359025/adding-to-the-end-of-list-in-lisp

(export '(catch-values
	  catch-value
	  catch-once
	  push-to-end))

(defmacro read-string (str)
  `(read (make-string-input-stream ,str)))
(defmacro eval-string (str)
  `(eval (read-string ,str)))
|#
(defvar current-text-context nil)
(defvar *text-input-state* nil)

(defstruct tamias-variable
  type
  value);;for use with editable non-string fields

(defstruct tamias-string
  (text "")
  (buffer nil)
  (newline-allowed t))

(defstruct (tamias-text (:include tamias-string))
  (position 0)
  ;;line-length
  column-limit
  (cursor-timer 0)
  (cursor-blink-off 40)
  (cursor-hide nil)
  (cursor-blink-on 80)
  (cursor-blinks 0)
  (cursor-blink-timeout 1200)
  (cursor-blink-stop nil)
  edited)

(defun cursor-blink-check (tamias-text)
  (incf (tamias-text-cursor-timer tamias-text))
  (if (>= (tamias-text-cursor-timer tamias-text) (tamias-text-cursor-blink-timeout tamias-text))
      (setf (tamias-text-cursor-blink-stop tamias-text) t)
      (setf (tamias-text-cursor-blink-stop tamias-text) nil))
  (when (not (tamias-text-cursor-blink-stop tamias-text))
    (when (> (tamias-text-cursor-timer tamias-text)
	   (+ (tamias-text-cursor-blink-off tamias-text)
	      (* (tamias-text-cursor-blink-on tamias-text)
		 (tamias-text-cursor-blinks tamias-text))))
      (setf (tamias-text-cursor-hide tamias-text) t))
    (when (> (tamias-text-cursor-timer tamias-text)
	     (+ (tamias-text-cursor-blink-on tamias-text)
		(* (tamias-text-cursor-blink-on tamias-text) (tamias-text-cursor-blinks tamias-text))))
      (setf (tamias-text-cursor-hide tamias-text) nil)
      (incf (tamias-text-cursor-blinks tamias-text)))))


(defmacro a<b<c (a n c)
  `(and (> ,n ,a)
	(< ,n ,c)))

(defmacro a<b<=c (a n c)
  `(and (> ,n ,a)
	(<= ,n ,c)))

(defmacro a<=b<=c (a n c)
  `(and (>= ,n ,a)
	(<= ,n ,c)))

(defmacro a>b>c (a n c)
  `(and (< ,n ,a)
	(> ,n ,c)))

(defmacro a>=b>=c (a n c)
  `(and (<= ,n ,a)
	(>= ,n ,c)))

(defmacro a>b>=c (a n c)
  `(and (< ,n ,a)
	(>= ,n ,c)))

(defmacro >b> (a n c)
  `(and (< ,n ,a)
	(> ,n ,c)))
	
(defmacro length> (sequence comparator)
  `(if (listp ,comparator)
       (> (length ,sequence) (length ,comparator))
       (> (length ,sequence) ,comparator)))

(defmacro length< (sequence comparator)
  `(if (listp ,comparator)
       (< (length ,sequence) (length ,comparator))
       (< (length ,sequence) ,comparator)))

(defmacro length= (sequence comparator)
  `(if (listp ,comparator)
       (= (length ,sequence) (length ,comparator))
       (= (length ,sequence) ,comparator)))

(export '(a<b<c
	  a<=b<=c
	  a>b>c
	  a>=b>=c
	  >a>
	  length>
	  length<
	  length=))


(defun reset-position (&optional (num 1))
  (setf (tamias-text-position current-text-context) num))

(defmacro set-hash ((key h-table) value)
  `(setf (gethash ,key ,h-table) ,value))

(defmacro get-hash (key h-table &optional default)
  `(gethash ,key ,h-table ,default))

(defmacro with-hash (hash-table &body body)
  `(flet ((set-hash (&rest arg-pairs)
		    (loop :for (key value) :on arg-pairs :by #'cddr
			  :do (setf (gethash key ,hash-table) value)))
	  (get-hash (key)
	    (gethash key ,hash-table)))
       ,@body))

(defmacro define-enum (&rest consts)
  `(loop :for const :in ',consts
      :do (let ((const-name (car const)) (const-val (cadr const))) (print const-name)
	      (eval `(defparameter ,const-name ,const-val)))))

;;(tam-enum (top-left 0) (top-right 1) (center 2) (bottom-left 3) (bottom-right 4))

;;selector.cl
#|
(defmacro cycle-selection (selection direction &key (min 0) max)
  `(let ((down :down)
	 (up :up)
	 (selection ,selection)
	 ;;selection ,selection because ,selection could be (ui-selector (gethash 1 (gethash :idle (gethash :gui-tester ui-managers)))), and evaluating that multiple times can cause performance issues.
	 (max ,max)
	 (min ,min))
     (if (or (eq ,direction 1)
	     (eq ,direction down))
	 (incf selection)
	 (when (or (eq ,direction -1)
		   (eq ,direction up))
	   (decf selection)))
     (when max
       (when (> selection max)
	 (setf selection max)))
     (when (< selection min)
       (setf selection min))
     (setf ,selection selection)))
|#
