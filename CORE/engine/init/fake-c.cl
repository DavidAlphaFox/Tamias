(cl:defpackage :C
  (:export ++
	   --
	   -=
	   =))
(cl:in-package :c)

(cl:defmacro ++ (var cl:&optional (delta 1))
  `(cl:incf ,var ,delta))

(cl:defmacro -- (var cl:&optional (delta 1))
  `(cl:decf ,var ,delta))

(cl:defmacro -= (var cl:&optional (delta 1))
  `(cl:decf ,var ,delta))

(cl:defmacro += (var cl:&optional (delta 1))
  `(cl:decf ,var ,delta))

(cl:defmacro = (var cl:&optional (val cl:nil))
  `(cl:if ,val
	  (cl:setf ,var ,val)
	  ,var))

(cl:in-package :cl-user)

