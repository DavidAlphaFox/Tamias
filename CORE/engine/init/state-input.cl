#|
Tamias Input Variable System
|#

#|
Note: Have both "international" and English based inputs
international will use scancodes and English will use the current input style per sdl2
|#
(in-package :states)

(defvar inputs-table (make-hash-table))
(setf (gethash :keyboard inputs-table) (make-hash-table))
(setf (gethash :mouse inputs-table) (make-hash-table))

(defvar top-level-input-table (make-hash-table))
(defvar modifiers-table (make-hash-table))
;;(defvar mouse-input-keywords (make-hash-table))

(setf (gethash :keyboard top-level-input-table) (make-hash-table))
(setf (gethash :nil modifiers-table) #b0)
(setf (gethash :down modifiers-table) #b1)
(setf (gethash :d modifiers-table) #b1)
(setf (gethash :hold modifiers-table) #b10)
(setf (gethash :h modifiers-table) #b10)
(setf (gethash :held modifiers-table) #b10)
(setf (gethash :moving modifiers-table) #b100)
(setf (gethash :move modifiers-table) #b100)
(setf (gethash :m modifiers-table) #b100)
(setf (gethash :up modifiers-table) #b1000)
(setf (gethash :release modifiers-table) #b1000)

(setf (gethash :shift modifiers-table) #b10000)
(setf (gethash :s modifiers-table) #b10000)
(setf (gethash :^ modifiers-table) #b10000)
(setf (gethash :ctrl modifiers-table) #b100000)
(setf (gethash :control modifiers-table) #b100000)
(setf (gethash :c modifiers-table) #b100000)
(setf (gethash :meta modifiers-table) #b1000000)
(setf (gethash :alt modifiers-table) #b1000000)
(setf (gethash :a modifiers-table) #b1000000)
(setf (gethash :hyper modifiers-table) #b10000000)

(defun kb-key-masks ()
  (let ((mod-state 0))
    (when (cl-user:shift-t)
      (incf mod-state (gethash :shift modifiers-table)))
    (when (cl-user:ctrl-t)
      (incf mod-state (gethash :ctrl modifiers-table)))
    (when (cl-user:alt-t)
      (incf mod-state (gethash :alt modifiers-table)))
    mod-state))

(defstruct tamias-input-variable
  (type :nil :type keyword)
  (state :nil :type keyword)
  (modifier-state #b0)
  (timer (timer:make :end 2))
  (quit-timer (timer:make :end 60 :in-seconds? t))
  (table (make-hash-table) :type hash-table))


(defun process-active-inputs ()
  ;;active-inputs need to be reset to hold tamias-input-variables rather than type - key pairs
  (when (gethash (state-sub-state states:state) (state-held-inputs states:state))
    (loop :for input-variable :in (gethash (state-sub-state states:state) (state-held-inputs states:state))
	  :do (let ((true-input-state
		      (+ (ash (ash (tamias-input-variable-modifier-state input-variable) -4) 4)
			 (gethash (tamias-input-variable-state input-variable)
				  modifiers-table))))
		(case (tamias-input-variable-state input-variable)
		  (:down
		   (timer:increment (tamias-input-variable-timer input-variable))
		   (when (timer:end? (tamias-input-variable-timer input-variable))
		     (setf (tamias-input-variable-state input-variable) :held)
		     (timer:reset-timer (tamias-input-variable-timer input-variable))
		     (decf (tamias-input-variable-modifier-state input-variable) #b1)
		     (incf (tamias-input-variable-modifier-state input-variable) #b10)
		     ))
		  (:held
		   (if (gethash (state-symbol states:state)
				(tamias-input-variable-table input-variable))
		       (if (gethash (state-sub-state states:state)
						 (gethash (state-symbol states:state)
							  (tamias-input-variable-table input-variable)))
			   (loop :for fn
				   :in (gethash true-input-state
						(gethash (state-sub-state states:state)
							 (gethash (state-symbol states:state)
								  (tamias-input-variable-table input-variable))))
				 :do (eval fn))))
		   (timer:increment (tamias-input-variable-quit-timer input-variable))
		   (when (timer:end? (tamias-input-variable-quit-timer input-variable))
		     (setf (tamias-input-variable-state input-variable) :nil)))
		  (otherwise
		   (if (gethash (state-symbol states:state)
				(tamias-input-variable-table input-variable))
		       (if (gethash (state-sub-state states:state)
				    (gethash (state-symbol states:state)
					     (tamias-input-variable-table input-variable)))
			   (loop :for fn
				   :in (gethash true-input-state
						(gethash (state-sub-state states:state)
							 (gethash (state-symbol states:state)
								  (tamias-input-variable-table input-variable))))
				 :do (eval fn))))
		   (setf (gethash (state-sub-state states:state) (state-held-inputs states:state))
			 (remove input-variable (gethash (state-sub-state states:state) (state-held-inputs states:state))))
		   ))))))
(export 'process-active-inputs)

(defun make-tamias-kb-input ()
  (make-tamias-input-variable :type :keyboard))

(defun make-tamias-mouse-input ()
  (make-tamias-input-variable :type :mouse))


#|
So, (gethash [input-"class"] input-system)
Then, we have the actual key and it's modifiers applied
|#

(define-condition invalid-tamias-input-variable-type (error) 
  ((argument :reader invalid-type-arg :initarg :type))
  (:report (lambda (condition stream)
             (format stream "~S is an invalid tamias-input-variable type. ~%Maybe you didn't initialize your input type?"
                     (invalid-type-arg condition)))))


#|
This has been a massive pain in the ass lol

TODO: 
Modify Add-key / add-mouse to use define-input as the "backend"
Modify key / mouse handler to use updated code (will allow for quite a few interesting combinations to occur)

Because of how the code is setup this will be easy and somewhat trivial. There'll be a bit of elbow grease involved, but nothing too terrible :)


Need to redo the remap-key function to remap-input


I think this might be the best way to handle input agnostically, or, more specifically, on a PC
A PC can have a multitude of peripherals attached, game consoles tend to have a /very/ limited number of peripherals even available. Sometimes there might be "gimmicks" like the steering wheel for the N64, but those are usually coded per game, rather than as a standardized specification within the system, so to speak (i.e. given a controller ID, a game/program can have specialized functions for non-standard buttons)
I'm honestly mostly talking out my ass, this is all just guess work tbh
This is extensible and easily customizable
|#

(defmacro define-input (input-type (state sub-state) (input-symbol/key &rest modifiers) body)
  `(let ((modifiers-key 0)
	 (state ,state)
	 (input-symbol/key ,input-symbol/key))
     (when (not (keywordp input-symbol/key))
       (setf input-symbol/key (aux:to-keyword ,input-symbol/key)))	   
     (when (not (gethash ,input-type inputs-table))
       (error 'invalid-tamias-input-variable-type :type ,input-type))
     (let ((input-variable (gethash input-symbol/key (gethash ,input-type inputs-table))))
       (when (not input-variable)
	 (setf (gethash input-symbol/key (gethash ,input-type inputs-table))
	       (make-tamias-input-variable :type ,input-type)
	       input-variable (gethash input-symbol/key (gethash ,input-type inputs-table))))
       (let ((tv-input-table (tamias-input-variable-table input-variable)))
	 (when (not (keywordp ,state))
	   (setf state (aux:to-keyword ',state)))
	 (if (not (gethash state tv-input-table))
	     (setf (gethash state tv-input-table)
		   (make-hash-table)
		   (gethash ,sub-state (gethash state tv-input-table))
		   (make-hash-table))
	     (if (not (gethash ,sub-state (gethash state tv-input-table)))
		 (setf (gethash ,sub-state
				(gethash state tv-input-table))
		       (make-hash-table))))
	 (if (numberp (car ',modifiers))
	     (setf modifiers-key (car ',modifiers))
	     (loop :for modifier :in ',modifiers
		   :do (when modifier
			 (let ((mod (gethash modifier modifiers-table)))
			   (if mod
			       (incf modifiers-key mod))))))
	 (setf (gethash modifiers-key
			(gethash ,sub-state (gethash state tv-input-table)))
	       ,body)))))
(export 'define-input)


(defun get-input (input-type input-symbol/key
		  &optional (modifier-key-hash 0)
		    (state states:state) (sub-state (state-sub-state states:state)))
  (let ((input-variable (gethash input-symbol/key (gethash input-type inputs-table)))
	(state-keyword (state-symbol state)))
    (when input-variable
      (let ((input-state-table (tamias-input-variable-table input-variable)))
	(case (mod modifier-key-hash #b10000)
	  (#b1 (when (not (eq (tamias-input-variable-state input-variable) :held))
		 (setf (tamias-input-variable-state input-variable) :down)))
 	  (#b100 (setf (tamias-input-variable-state input-variable) :move))
	  (#b1000 (setf (tamias-input-variable-state input-variable) :release)))
	(when (eq (tamias-input-variable-state input-variable) :down)
	  (setf (tamias-input-variable-modifier-state input-variable) modifier-key-hash)
	  (if (not (find input-variable (gethash sub-state (state-held-inputs state))))
	      (push input-variable (gethash sub-state (state-held-inputs state)))))
	(when (gethash state-keyword input-state-table)
	  (when (gethash sub-state (gethash state-keyword input-state-table))
	    (when (and (not (eq (tamias-input-variable-state input-variable) :held))
		       (not (eq (tamias-input-variable-state input-variable) :nil)))
	      (setf (tamias-input-variable-modifier-state input-variable) modifier-key-hash)
	      (if (> modifier-key-hash 0)
		  (gethash modifier-key-hash (gethash sub-state (gethash state-keyword input-state-table))))))
	      ;;(setf (gethash ,input-state (tamias-input-variable-states-table input-table))
	      ;;      (make-hash-table))
	  )))))
(export 'get-input)

(defun get-input-state (input input-type)
  ;;state/sub-state agnostic, this is the current physical state of the input, regardless of where it's called from
  (tamias-input-variable-state
   (gethash input (gethash input-type inputs-table))))
(export 'get-input-state)


(defun get-key-state (input)
  (get-input-state input :keyboard))
(export 'get-key-state)

(defmacro key-state (input)
  `(get-key-state ,input))
(export 'key-state)


(defun get-mouse-state (input)
  (get-input-state input :mouse))
(export 'get-mouse-state)

#|
(defmacro get-input-state-table (input-table input-symbol/key input-state modifier-key-hash)
  `(let ((input-variable (gethash ,input-symbol/key ,input-table)))
     (when input-variable
       (tamias-input-variable-states-table input-variable))))
(export 'get-input-state-table)
|#

(defmacro define-top-level-input (input-type (input-symbol/key &rest modifiers) &body body)
  `(let ((modifiers-key 0)
	 (tl-input-table (gethash ,input-type top-level-input-table))
	 (input-symbol/key ,input-symbol/key))
     (when tl-input-table
       (loop :for modifier :in ',modifiers
	     :do (let ((mod (gethash modifier modifiers-table)))
		   (if mod
		       (incf modifiers-key mod))))
       (let ((input-variable (gethash input-symbol/key tl-input-table)))
	 (when (not input-variable)
	   (setf (gethash input-symbol/key tl-input-table)
		 (make-tamias-input-variable :type ,input-type)))
	 (setf (gethash modifiers-key
			(tamias-input-variable-table (gethash input-symbol/key tl-input-table)))
	       ',body)))))

(defun process-top-level-input (input-type input-symbol/key modifiers-bit-mask)
  (let ((tl-input-table (gethash input-type top-level-input-table)))
;;    (incf modifiers-bit-mask (gethash input-state modifiers-table))
  
    (if (gethash input-symbol/key tl-input-table)
	(eval (car (gethash modifiers-bit-mask (tamias-input-variable-table (gethash input-symbol/key tl-input-table))))))))

(define-top-level-input :keyboard (:|`| :down)
  (tamias:console.toggle)) ;;eventually, we also want to send the engine into it's own console sub-state where text can be input and evaluated during gameplay
;;we also want to "pause" gameplay when the console is activated
;;meaning, on the first frame, we take a screen-shot of the current gameworld and render it behind the console
;;and then, anytime a command is evaluated, we let the render loop go once and then take a new screen-shot
;;
;;Actually, no, we don't take a "screen-shot"
;;We 'disable' the animation subsystem on console start and enable it on console-exit. It is the inverse of the console state.
;;I'll add some skeleton variables and stuff rn 12/8/23

(define-top-level-input :keyboard (:|`| :down :ctrl)
  (setf cl-user:exit-tamias? t))

;;(define-top-level-input :keyboard (:|`| :down :shift)
;;  (tamias:console.add-message (kb-key-masks)))

