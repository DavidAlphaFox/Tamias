(defstruct stats
  (attack 10)
  (attack-mod 0)
  (defense 0)
  (defense-mod 0)
  (range-attack 10)
  (range-attack-mod 0)
  (magic-attack 10)
  (magic-attack-mod 0)
  (magic-defense 10)
  (magic-defense-mod 0)
  (agility 1) ;;;;Higher goes first, lower goes last
  (agility-mod 0)
  (speed 1) ;;;;in Rogue-like engines, this will be how many spaces an entity can move
  (speed-mod 0)
  (dodge 1)
  (dodge-mod 0)
  (hp 40)
  (max-hp 40)
  (mp 40)
  (max-mp 40)
  (xp 0)
  (level 1)
  (elemental 'entity)
  (status 'alive)
  extra-effect ;;;;i.e.
  (line-of-sight 10) ;meaning, it can see or ''see'' x amount of tiles away
  current-action)

#|
(defstruct (entity (:include t-object))
  name
  (stats (make-stats))
  weapon
  armor
  (symbol "E")
  (bg-color tamias.colors:+black+)
  (symbol-color tamias.colors:+white+)
  (score 0))
|#
(loop for stat in '(attack
		    attack-mod
		    defense
		    defense-mod
		    range-attack
		    range-attack-mod
		    magic-attack
		    magic-attack-mod
		    magic-defense
		    magic-defense-mod
		    agility
		    agility-mod
		    speed
		    speed-mod
		    dodge
		    dodge-mod
		    hp
		    max-hp
		    mp
		    max-mp
		    xp
		    level
		    elemental
		    status
		    extra-effect
		    line-of-sight
		    current-action)
   do (let ((func-name (intern (string-upcase (concatenate 'string "entity-" (symbol-name stat)))))
	    (replaced-func (intern (string-upcase (concatenate 'string "stats-" (symbol-name stat))))))
	(eval `(defmacro ,func-name (entity)
		 `(,',replaced-func (entity-stats ,entity))))))
