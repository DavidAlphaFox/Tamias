(defvar layer-id -1)
(defvar scene-id -1)
(defvar area-id -1)
(defvar world-id -1)

(defstruct camera
  (dimensions ( vector 0 0 tamias:screen-width tamias:screen-height ))
  ;;[ x/tile-x  y/tile-y  width  height ]
  #|
  (x 0) (y 0) ;;more for a platformer, or an RPG that doesn't strictly move by tiles
  (tile-x -1) (tile-y -1) ;;easily better for RPGs (I think)
  (width-px tamias:screen-width) (height-px tamias:screen-height)
|#
  (zoom-factor 1))

(defmacro camera-x (camera)
  `(aref ( camera-dimensions ,camera )  0 ))
(defmacro camera-y (camera)
  `(aref ( camera-dimensions ,camera )  1 ))
(defmacro camera-tile-x (camera)
  `(aref ( camera-dimensions ,camera )  0 ))
(defmacro camera-tile-y (camera)
  `(aref ( camera-dimensions ,camera )  1 ))
(defmacro camera-width (camera)
  `(aref ( camera-dimensions ,camera )  2 ))
(defmacro camera-height (camera)
  `(aref ( camera-dimensions ,camera )  3 ))



(defstruct spawn-point
  (area (vector 0 0 16 16))
  randomized?    (randomized-area (vector 10 10 20 20)) ;;r-area: '(w-min h-min w-max h-max)
  timer   trigger   inhibitor    max-at-frame max-generated
  spawn-func ;;is a string, "(make-[ENTITY-TYPE])"
  entity-type ;;is a keyword
  )
;;randomized will be either nil or #(x-mod y-mod w-mod h-mod)
(defmacro spawn-point-x (spawn-point)
  `(elt (spawn-point-area ,spawn-point) 0))
(defmacro spawn-point-y (spawn-point)
  `(elt (spawn-point-area ,spawn-point) 1))
(defmacro spawn-point-width (spawn-point)
  `(elt (spawn-point-area ,spawn-point) 2))
(defmacro spawn-point-height (spawn-point)
  `(elt (spawn-point-area ,spawn-point) 3))



(defstruct tamias-map ;;tile-based
  file
  (data (make-array '(1 1) :initial-element -1)));;should -1 be no impedence? or should that be 0?

(defstruct (tile-map ( :include tamias-map ))
  (sheet ( sprite:make-sheet )) ;;sheet is for gfx
  )
;;load tile-map, data = read tile-map file
;;or use a loop read-line (for the 'column' intro) and then
;;       loop read integer from line, (aref y x data) (incf x) . outer: (incf y)



(defstruct layer
  bg  tile-map  objects-ids
  ;;Each layer has it's own objects-ids list. This list of ids is used in conjunction with the parent scene's objects hash table (scene-objects scene). This allows for objects to be encapsulated within a scene to be rendered below other objects, but still allow them to have collision
  spawn-points;;A list/array of spawn points
  UI? ;;If UI?, then use the GUI manager subsystem (possibly adding in world->A->S->layer support is a bad idea tbh. Well, maybe not.)
  size ;;width and height
  >x >y ;;used to send entities to different layers. It's a way around incorrect rendering. And since a player will inherently be an entity, it means you don't need to write special code for a player (albeit it doesn't prevent one from doing so)
  ;; writing this on 29/11/23: I don't remember what >x and >y are supposed to do. I legitimately don't remember (side effect of concussion?)
  visible 
  (id (incf layer-id)))



(defstruct portal
  x y width height
  player-pos-new
  to-scene ;;is a scene-id
  active?)



(defstruct scene
  (physics '((:collision 1))) ;;((:wind id-in-array) (:ice id-in-array))
  (physics-map (make-tamias-map))
  ;;need to either add a seamless transition to another scene, or modify how scenes and layers work
  collision-queue
  (objects (make-hash-table :size 60))
  (camera (make-camera)) ;;used to determine where the tile-map is being rendered from
  (layers (make-hash-table))
  (state :idle) ;; as opposed to :cutscene, :init, :fade, etc.
  layer-ids
  portals ;;this may become a quad-tree in itself, just one level deep
  (id (incf scene-id)))

;;

(defstruct area
  music ( state :idle )
  (scenes ( make-hash-table ))   scene-ids
  (scene -1 )   (id ( incf area-id )))

(defstruct world
  physics
  (areas ( make-hash-table ))
  area-ids
  (area -1 )
  (id ( incf world-id )))

(define-symbol-macro current-world (state-world tamias:state))
;;current-x are objects or symbol macros lol
(define-symbol-macro current-area (gethash (world-area current-world) (world-areas current-world)))
(define-symbol-macro current-scene (gethash (area-scene current-area) (area-scenes current-area)))

(defvar worlds (make-hash-table))
(defvar worlds-ids nil)


