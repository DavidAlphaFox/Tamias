(defstruct track
  path
  stream
  (timer (timer:make :end 1200 :in-seconds? t))
  (loop-point 0.0))

(defmacro define-track (track path &key (loop-point 0.0))
  `(defvar ,track (make-track :path ,path :loop-point ,loop-point)))

#|
#+(or win32 windows)
(load "engine/audio/backends/sdl2.cl")
#-(or win32 windows)
(load "engine/audio/backends/sdl2.cl")
|#

;;(load "backends/mpd.cl")


#|
int music_internal_position(double position)
{
if (music_playing->interface->Seek) {
return music_playing->interface->Seek(music_playing->context, position);
}
return -1;
}
|#

#|
A note about the music subsystem
Basically, I want to set it up to where when you change state, it saves the current position of the current track and resumes the track from that point when you return to that state
I.e. The overworld music resumes from where it was when you leave a battle
Or the Town music starts up where it paused when you exit the shop

So, this means that tamias needs to get the position of the current track and save it somewhere. That somewhere is what I'm trying to decide on
|#

