#|
==============================================================================
                           SPRITES and ANIMATION
==============================================================================
|#

(in-package :sprite)

#|
(defmacro optimize-sheet (var)
  `(setf (sprite:sheet-texture ,var) (sdl2:create-texture-from-surface tamias:renderer (sheet-surface ,var))))
|#

(defmacro set-sheet-texture (var texture)
  `(setf (sprite:sheet-texture ,var) ,texture))

(defmacro set-sheet-surface (sheet surface)
  `(setf (sprite:sheet-surface ,sheet) ,surface))

;;(sdl2:surface-width surface))
;;(sdl2:surface-height surface))
(defun load-sheet (sheet &optional surface?)
  (let* ((surface (sheet-texture sheet)))
    (set-sheet-height sheet (sdl2:texture-height surface))
    (set-sheet-width sheet (sdl2:texture-width surface))
    (set-cells sheet)
    (if surface?
	(set-sheet-surface sheet surface)
	(set-sheet-texture sheet surface))))

(defun free-sheet (entity)
  (let ((sheet (tamias.entities:entity-sprite-sheet entity)))
    (if (sheet-texture sheet)
	(sdl2:destroy-texture (sheet-texture sheet))
	(sdl2:free-surface (sheet-surface sheet)))
  ;;  (setf sheet nil) needs to be added into a macro, i.e. free-asset or something like that
  ;; in fact, (push-quit ...) could work
    ))


#|
usage:
(defsheet bat "game/gfx/bat-sheet.png" '(cell-width cell-height))
then (entity-sheet bat) for the bat's sheet
also, the idea is that "entity" is a generalized structure in which all of your entity structs
are based on, mainly to help in reducing necessary labour time
|#
