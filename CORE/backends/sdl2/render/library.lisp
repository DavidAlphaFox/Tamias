(defun update-window-size ()
  (setf tamias:screen-width (elt (nth tamias:resolution tamias:resolution-list) 0)
	tamias:screen-height (elt (nth tamias:resolution tamias:resolution-list) 1))
  (sdl2:set-window-size tamias:default-window tamias:screen-width tamias:screen-height)
;;  (gl:viewport 0 0 tamias:screen-width tamias:screen-height)
  )

(defun tamias:load-image (file-path &optional surface?)
  (if surface?
      (sdl2-image:load-image file-path)
      (sdl2:create-texture-from-surface tamias:renderer (sdl2-image:load-image file-path))))

(defun tamias:make-rect (x y width height)
  (sdl2:make-rect x y width height))

(defmacro tamias:free-image (img)
  `(sdl2:destroy-texture ,img))


(in-package :render)
(export 'screenshot)
#|
==============================================================================
                                  GENERAL
==============================================================================
|#

(defun draw-line (x y x2 y2 &optional (color '(255 255 255 255)))
  (let ((r (elt color 0))
	(g (elt color 1))
	(b (elt color 2))
	(a (elt color 3)))
    (sdl2:set-render-draw-color tamias:renderer r g b a)
    (sdl2:render-draw-line tamias:renderer x y x2 y2)))


(defun create-texture (&key (format sdl2:+pixelformat-rgba8888+) (access 0) (width 16) (height 16) (color tamias.colors:+white+))
  (sdl2:set-render-draw-color tamias:renderer (elt color 0) (elt color 1) (elt color 2) (elt color 3))
  (sdl2:render-clear tamias:renderer)
  (sdl2:create-texture tamias:renderer format access width height))

(defmacro with-rectangle (name rect-parameters &body body)
  `(let ((,name (sdl2:make-rect (car ,rect-parameters)
				(cadr ,rect-parameters)
				(caddr ,rect-parameters)
				(cadddr ,rect-parameters))))
     ,@body
     (sdl2:free-rect ,name)))

(defun screenshot ()
  (let* ((w tamias:window-width)
	 (h tamias:window-height)
	 (s-shot (sdl2:create-rgb-surface w h 32)))
    (with-rectangle screen-rect (list 0 0 w h)
      (sdl2-ffi.functions:sdl-render-read-pixels
       tamias:renderer screen-rect
       ;;sdl2:+pixelformat-rgba8888+
       sdl2:+pixelformat-argb8888+
       (sdl2:surface-pixels s-shot) (sdl2:surface-pitch s-shot))
      (sdl2-image:save-png s-shot "screenshot.png"))
    (sdl2:free-surface s-shot)
  ))

#|
usage: (with-rectangle bat-rect (list (bat-x bat) (bat-y bat) (bat-width bat) (bat-height bat))
(render:rectangle bat-rect))
|#

(defmacro create-rectangle (rect-vals)
  `(sdl2:make-rect (car ,rect-vals)
		   (cadr ,rect-vals)
		   (caddr ,rect-vals)
		   (cadddr ,rect-vals)))
#|
usage: (defun some-func ()
          (let ((my-rect (create-rectangle '(x y width height))))
            (render:rectangle my-rect)
            (render:free-rectangle my-rect)))
|#

(defmacro free-rectangle (rect)
  `(sdl2:free-rect ,rect))

(defun image (img &optional (x 0) (y 0) w h)
  (if img
      (let ((src (sdl2:make-rect 0 0 (sdl2:texture-width img) (sdl2:texture-height img)))
	    (dest (sdl2:make-rect x y
				  (or w (sdl2:texture-width img))
				  (or h (sdl2:texture-height img)))))
	(sdl2:render-copy-ex tamias:renderer
			     img
			     :source-rect src
			     :dest-rect dest)
	(free-rectangle src)
	(free-rectangle dest))))

(defun tex-blit (tex &key src dest color angle center (flip :none))
  (if color
      (progn (sdl2:set-texture-color-mod tex (elt color 0) (elt color 1) (elt color 2))
	     (if (eq (length color) 4)
		 (sdl2:set-texture-alpha-mod tex (elt color 3)))))
  (let ((src (or src (sdl2:make-rect 0 0 (sdl2:texture-width tex) (sdl2:texture-height tex)))))
    (sdl2:render-copy-ex tamias:renderer
			 tex
			 :source-rect src
			 :dest-rect dest
			 :angle angle
			 :center center
			 :flip (list flip))
    (sdl2:free-rect src)
    (sdl2:free-rect dest))
#|
      (progn (sdl2:render-copy-ex tamias:renderer
				  tex
				  :source-rect src
				  :dest-rect dest
				  :angle angle
				  :center center
				  :flip (list flip))
	     (sdl2:free-rect src)
	     (sdl2:free-rect dest))
|#
  )

(defun box (x y w h &optional (color (vector 255 255 255 255)) (filled t))
  (if (not (or (vectorp color)
	       (listp color)))
      (setf color (eval color)))
  (let* ((r (elt color 0))
	 (g (elt color 1))
	 (b (elt color 2))
	 (a (or (elt color 3)
		255))
	 (rect (sdl2:make-rect x y w h))) ;; I still want to figure out a way to better manage memory with Tamias, currently, a rectangle is made and then freed every frame
    ;;set the render draw color to the color
    (sdl2:set-render-draw-color tamias:renderer r g b a)
    (if filled
	(sdl2:render-fill-rect tamias:renderer rect) ;;fills the area of x -> w and y -> h with color (default white)
	(sdl2:render-draw-rect tamias:renderer rect)) ;;draws 4 lines, x -> , x -> w, y -> w, h -> w with color (default white)
    (sdl2:free-rect rect))) ;;frees memory taken up by the rect. Not doing this will cause a segfault

(defun rectangle (rect &optional (color (vector 255 255 255 255)) filled)
  (box (elt rect 0) (elt rect 1)
       (elt rect 2) (elt rect 3)
       color filled))


(defun pixel (x y &optional (color (vector 255 255 255 255)))
  (box x y 1 1 :color color))

#|(defun calculate-arc-slope (x1 y1 x2 y2)
  (let ((rise (- y2 y1))
	(run (- x2 x1))
	slope-case)
    (if (< rise 0)
	;;going up
	    ;;going left
	    ;;going right
	;;going down
	    ;;going left
	    ;;going right
	)
    (case slope-case
      (SE );;midpoint C = + (/ run 2) x1 :: + (/ rise 2) y1
      (SW );; C = + (/ run 2) x1 :: + (/ rise 2) y1
      (NW );; C = + (/ run 2) x1 :: + (/ rise 2) y1
      (NE );; C = + (/ run 2) x1 :: + (/ rise 2) y1
      ) ;;NOTE: I may be able to just use C = ... for the midpoint regardless of direction
    )
  )

(defun circle (cx cy r &key filled)
  ;;draw 4 arcs
  (let ((x1 cx)
	(x2 (+ cx r))
	(x3 (- cx r))
	(y1 cy)
	(y2 (- cy r))
	(y3 (+ cy r)))
    ))

|#
#|
In the end, the goal is to get a list of points, and to use that list to draw a circle
Interestingly, because of how I'm getting the "slope", I don't need to reverse the list of the slope
Instead, I can apply that list in the opposite direction
I.e., if a list is going right-down, +X +Y then when it would be going up-left, -Y -X
  |#  
    ;;draw order: x1::y2  x2::y1  x1::y3  x3::y1
    ;;filled uses lines, not-filled goes along a list of points and fills in those points
    ;;ignore points that are past x + r, x -r, y + r and y - r
    ;;or x - r <= x <= x + r and y -r <= y <= y + r
    ;;general idea is that from x1::y2 to x2::y1 would be a "long less-long middle less-middle one" pattern, then reverse that pattern "one ... middle...long"
    ;;Where the x slope goes down, while y slope does not change, so x begins as 6, then 5, 4 and so on. then the pattern that started with x is reversed and is applied to y
    ;;while the x slope "=" 1, just like y.
    ;;so, pseudocode:
    #|
    while x-slope > 0
    loop below x-slope
    do incf x-pixel
       collect pair x-pixel y-pixel
       END
    incf y-pixel
    decf x-slope
    END REP
    |#
    
  
#|

Another methodology for drawing a curved arc:
First get the slope between two points in a rectangle
We'll call these two points, which are the beginning and end of an arc, A and B
After getting the slope of A and B, determine the midpoint, which will be called C
Create a new point D that is a straight line from C, so as to be perpendicular to line AB
Determine the midpoint of CD, E
Determine slope of AE, where the Angle of CAD is 45 degrees
Divide the slope of X of AE by the y slope of AE
This is the amount of X steps required to create a curved arc in the X direction
Upon completion of the X steps, switch to Y steps
Determine an evenly spaced out step amount, where it is something like (4 2 1)
Move 4 in the x direction, rendering a pixel per point
Move 1 in the y direction, render
Move 2 in X, render
Move 1 in y
Move 1 in X
Reverse the list, and switch to Y steps
upon completion of the X and Y steps, a curved line will be drawn

Drawing a circle:
Arc method, but upon completion of Arc 1, reverse X direction
Arc 2, reverse y
Arc 3, reverse X
Arc 4 will be the completed circle

Example:
        A----------------------------D
         \--\  	         	 --/
	  \  -----\   	      --/
	   \	   ---\     -/
            \	       ---E/
	     \	      --/|
	      \	   --/	 |
	       \C-/   	/
                \     	|
		 \    	|
		  \    /
		   \   |
		    \ /
		     \|
                      B
|#
