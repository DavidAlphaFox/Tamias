(defun trap-mouse-in-window ()
  (sdl2:set-relative-mouse-mode t)
  (sdl2:hide-cursor))
;;call free mouse on state change
(defun free-mouse-from-window ()
  (sdl2:set-relative-mouse-mode nil)
  (sdl2:show-cursor))
;;Justification for this instead of snapping mouse to window on window creation:
;;possible issues with winowing frameworks.


#|  (if (gethash :move (state-mouse (eval tamias:state)))
      (loop for func in (gethash :move (state-mouse (eval tamias:state)))
do (eval func))))|#


