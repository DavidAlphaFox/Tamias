(defun quit-game ()
  (sdl2:push-event :quit))

#|
(defun go-to-options ()
  (setf tamias:selection 0)
  (setf tamias:sub-state 'options))


(defvar *selection-column* 0)
(defvar *selection-row* 0)
(defun change-selection (d &key (max-row 2) (max-column 1))
  (case d
    ((0 up) (if (> *selection-row* 0)	   
		(decf *selection-row* 1)
		(setf *selection-row* max-row)))
    ((1 right) (if (< *selection-column* max-column)
		  (incf *selection-column* 1)
		  (setf *selection-column* 0)))
    ((2 down) (if (< *selection-row* max-row)
		  (incf *selection-row* 1)
		  (setf *selection-row* 0)))
    ((3 left) (if (> *selection-column* 0)
		   (decf *selection-column* 1)
		   (setf *selection-column* max-column)))))
|#

(defun modifier-key? (key &optional value)
  (let ((key-up? (eql value :up)))
    (setf value (not key-up?))
    (case key
      ((:SCANCODE-RCTRL :SCANCODE-LCTRL :scancode-capslock)
       (setf (modifiers-control modifiers) value)
       t)
      ((:SCANCODE-RSHIFT :SCANCODE-LSHIFT)
       (setf (modifiers-shift modifiers) value)
       t)
      ((:SCANCODE-RALT :SCANCODE-LALT)
       (setf (modifiers-meta modifiers) value)
       t)
      (otherwise nil))))

#|
(defun text-key-check (key)
  (declare (ignore key)))
|#

(defun exit-tamias? ()
  (if exit-tamias?
      (sdl2:push-event :quit))
  (setf exit-tamias? nil))
  
(let ((key-state :down))
  (defun keydown-check (key)
    (let ((key (intern (substitute #\- #\space (string-upcase key)) "KEYWORD")))
      (if current-text-context
	  (text-key-check key)
	  (progn (case key
	    ;;(:P (print modifiers))
	    ;;      (:Escape (quit-game))
#|
		   (:|`| (if (ctrl-t)
			     (progn (setf (modifiers-control modifiers) nil)
				    ;;(sdl2:push-event :quit)
				    (setf exit-tamias? t))
			     (if (shift-t)
				 (print modifiers)
				 (tamias:console.toggle))))
|#
		   (:PRINTSCREEN (if (ctrl-t)
				     (push '(render:screenshot) tmp-proc))))
		 (push-tamias-key-input :key key :k-state key-state))))))
  #|(and (not *text-input-state*)
	     (gethash key (gethash :down (state-keys (eval tamias:state)))))
	(loop for func in (gethash key (gethash :down (state-keys (eval tamias:state))))
     do (eval func)))))|#
     

#|
blinking cursor:
(defstruct text-cursor
  (bliink-counter 0)
  blink?
  current-vis)
(defvar blink-counter 0)
(defvar blink? nil)

(defun blinking-cursor ()
  (if (not blink?)
      (render:box [current-context-x] [current-context-y] 2 character-height :color tamias.colors:+black+)))
(defun blink-counter (cursor)
  (incf (text-curosr-blink-counter text-cursor))
  (if (>= blink-counter [frames])
      (setf blink-counter 0
	    blink? (not blink?))))

|#

(let ((key-state :up))
  (defun keyup-check (key)
    (let ((key (intern (substitute #\- #\space (string-upcase key)) "KEYWORD")))
      (push-tamias-key-input :key key :k-state key-state))))

