(defun load-image-font (font-path)
  (setf tamias:font (sdl2-image:load-image font-path)))

(defun tamias:load-font (&key (file-type :image) (font-path "CORE/assets/font.png"))
  (case file-type
    (:image (load-image-font font-path))
    (:ttf (tamias:console.add-message "Coming soon to VHS and DVD."))
    ))
			       
