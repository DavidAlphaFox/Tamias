(defun render.menu-bar-item (ui x-offset y-offset &key width active)
  (render:box x-offset y-offset
	      (or width (ui-menu-item-width ui))
	      (cadr tamias.string:character-size)
	      (or (ui-menu-item-color ui)
		  tamias.colors:+dark-red+))
  (if active
      (render:box x-offset y-offset
		  (or width (ui-menu-item-width ui))
		  (cadr tamias.string:character-size)
		  tamias.colors:+cobalt+)
      (if (eq ui.current-hover (ui-id ui))
	  (render:box x-offset y-offset
		      (or width (ui-menu-item-width ui))
		      (cadr tamias.string:character-size)
		      (tamias.colors:offset tamias.colors:+dark-red+))))
  (if (not (tamias-string-buffer (ui-label ui)))
      (progn (push (ui-buff-func ui) quit-functions)
	     (render:to-buffer (tamias-string-text (ui-label ui))
			       (tamias-string-buffer (ui-label ui)))))
  (render:text-buffer (tamias-string-buffer (ui-label ui))
		      nil
		      (list x-offset y-offset))
  ;;(render:text (tamias-string-text (ui-menu-item-label ui-element)) x-offset y-offset))
  )


(defmethod ui.render (ui-element (ui-type (eql :menu-bar)) &optional hover (x-offset 0) (y-offset 0))
  (declare (ignore hover))
  (let ((mbar ui-element))
    (render:box (ui-base-x mbar) (ui-base-y mbar)
		(ui-base-width mbar) (ui-base-height mbar)
		(or (ui-element-color mbar)
		    tamias.colors:+dark-red+))
    (let ((current-x-offset x-offset)
	  (current-y-offset y-offset)
	  (active-item (ui-menu-bar-active-item mbar)))
      (loop for item-id in (ui-menu-bar-ids mbar) ;;with each new item that's dealt with, increase the current-x-offset
	    do (let ((item (gethash item-id (ui-menu-bar-children mbar))))
		 (setf current-y-offset y-offset)
		 (if (not (eq item-id active-item))
		     (render.menu-bar-item item current-x-offset current-y-offset)
		     (progn (render.menu-bar-item item current-x-offset current-y-offset :active t)
			    (setf x-offset current-x-offset)
			    (let ((ui-menu (gethash active-item (ui-menu-bar-children mbar))))
			      (if (ui-menu-ids ui-menu)
				  (loop for ui-id in (ui-menu-ids ui-menu)
					do (let ((ui-item (gethash ui-id (ui-menu-children ui-menu))))
					     (incf current-y-offset (cadr tamias.string:character-size))
					     (render.menu-item ui-item current-x-offset current-y-offset
							       :width (ui-menu-item-widest item))))))
			    (setf current-x-offset x-offset)))
		 (incf current-x-offset (+ (ui-padding-x mbar) (ui-menu-item-width item))))))))


(defun render.menu-bar (state sub-state)
  (declare (ignore state sub-state))
  (let ((menu-bar (get-current-menu-bar)))
    (if menu-bar
	(ui.render menu-bar 'menu-bar))))

