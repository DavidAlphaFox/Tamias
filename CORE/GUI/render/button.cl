(defmethod ui.render (ui-element (ui-type (eql :button)) &optional hover (x-offset 0) (y-offset 0))
  (ui.render-bg ui-element
		(+ x-offset (ui-base-x ui-element))
		(+ y-offset (ui-base-y ui-element)))
  (when (eq (ui-id ui-element) hover)
      (ui.render-hover ui-element)))
