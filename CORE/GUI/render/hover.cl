(defun render.hover-bg (x y w h color)
  (if color
      (let ((r (elt color 0))
	    (g (elt color 1))
	    (b (elt color 2)))
#|	(if (> r 127)
	    (if (> r 191)
		(setf r (- 128 (- r 128)))
		(setf r (- r 64)))
	    (if (< r 63)
		(setf r (+ r r r)) ;;Add is faster than mult
		(setf r (+ r r))))
	(if (> g 127)
	    (if (> g 191)
		(setf g (- 128 (- g 128)))
		(setf g (- g 64)))
	    (if (< g 63)
		(setf g (+ g g g)) ;;Add is faster than mult
		(setf g (+ g g))))
	(if (> b 127)
	    (if (> b 191)
		(setf b (- 128 (- b 128)))
		(setf b (- b 64)))
	    (if (< b 63)
		(setf b (+ b b b)) ;;Add is faster than mult
		(setf b (+ b b))))
|#
	(render:box x y w h (list r g b 127)))
      (let ((color tamias.colors:+pastel-pink+))
	(render:box x y w h (list (car color) (cadr color) (caddr color) 127)))))
  

(defun ui.render-hover (ui-element &optional (x-offset 0) (y-offset 0) x y w h)
  (let ((x (+ x-offset (or x (ui-base-x ui-element))))
	(y (+ y-offset (or y (ui-base-y ui-element))))
	(w (or w (ui-base-width ui-element)))
	(h (or h (ui-base-height ui-element))))
    (render.hover-bg x y w h (ui-base-color ui-element))))
