(defmacro ui.active-ui ()
  `(ui-manager-current-active (ui.current-manager)))

(define-symbol-macro active-ui-element (ui.active-ui))
;;(defvar active-ui-element nil)
(defun ui.set-active (ui-e)
  (if (ui.current-manager)
      (setf active-ui-element ui-e)))
;;this is some stupid code lol

(defun ui.render-elements ()
  (let ((cur-manager (ui.current-manager)))
    (if cur-manager
	(let* ((ui-collection-ids (remove 'menu-bar (ui-manager-collection-ids cur-manager))))
	  (loop for id in ui-collection-ids
		do (let ((ui-element (gethash id (ui-manager-collection cur-manager))))
		     (ui.render ui-element
				(ui-type ui-element)
				ui.current-hover)))
	  (if (find 'menu-bar (ui-manager-collection-ids cur-manager))
	      (let ((ui-element (gethash 'menu-bar (ui-manager-collection
						    cur-manager))))
;;						    (gethash sub-state (gethash state ui-managers))))))
		(ui.render ui-element
			   (ui-type ui-element)
			   ui.current-hover)))))))
  ;;(render:text (write-to-string (ui-manager-current-hover (get-current-ui-manager)))
;;	       40 10)


(defmacro ui.color-check (el)
  `(when (length< (ui-color ,el) 3)
     (setf (ui-color ,el) (vector (elt (ui-color ,el) 0)
				      (elt (ui-color ,el) 1)
				      (elt (ui-color ,el) 2)
				      255))))

(defun gui:state.init-gui (state sub-state &optional ui-elements)
  (aux:push-to-end (list state sub-state) ui-managers-list)
  (if (not (gethash state ui-managers))
      (setf (gethash state ui-managers) (make-hash-table)))
  (if (not (gethash sub-state (gethash state ui-managers)))
      (setf (gethash sub-state (gethash state ui-managers))
	    (make-ui-manager))) ;;accidentally made it a hash-table and not a ui-manager
  (let ((cur-man (gethash sub-state (gethash state ui-managers))))
    (if ui-elements
	(loop for element in ui-elements
	      do (push (ui-id element)
		       (ui-manager-collection-ids
			cur-man))
		 (setf (gethash (ui-id element)
				(ui-manager-collection
				 cur-man))
		       element)))))

(defmacro state.init-ui (state sub-state &rest ui-elements)
  `(gui:state.init-gui (aux:to-keyword ',state) (aux:to-keyword ',sub-state) ',ui-elements))
#|(defun state.init-ui (state sub-state &rest ui-elements)
  "Use symbols for state and sub-state. ui-elements are not necessary"
  (if (not sub-state)
      (if (not (symbolp state))
	  (setf sub-state (state-sub-state state)
		state (state-symbol state))
	  (setf sub-state (state-sub-state tamias:state)))
      (if (not (symbolp state))
	  (setf state (state-symbol state))))
  (if (not (gethash state ui-managers))
      (setf (gethash state ui-managers) (make-hash-table)))
  (if (not (gethash sub-state (gethash state ui-managers)))
      (setf (gethash sub-state (gethash state ui-managers)) (make-ui-manager)))
  (if ui-elements
      (loop for element in ui-elements
	    do (push (ui-id element) (ui-manager-collection-ids (gethash sub-state (gethash state ui-managers))))
	       (setf (gethash (ui-id element) (ui-manager-collection (gethash sub-state (gethash state ui-managers))))
		     element))))
|#
(defun state.init-gui (state sub-state &rest ui-elements)
  (state.init-ui state sub-state ui-elements))

;;      (setf (ui-manager-collection (gethash sub-state (gethash state ui-managers))) ui-elements)))


     
(defmacro ui-element.add (state sub-state ui-el)
  `(progn (push (ui-id ,ui-el)
		(ui-manager-collection-ids (get-ui-manager (aux:to-keyword ,state) (aux:to-keyword ,sub-state))))
	  (setf (gethash (ui-id ,ui-el)
			 (ui-manager-collection
			  (get-ui-manager (aux:to-keyword ,state) (aux:to-keyword ,sub-state))))
		,ui-el)))
#|
(defun manager.init? (state sub-state)
  (if (gethash state ui-managers)
      (state.init-ui state sub-state)
      (if (not (gethash sub-state (gethash state ui-managers)))
(state.init-ui state sub-state))))|#
(defun manager.init? (state sub-state)
  (state.init-ui state sub-state))

(defmacro ui.add-to-manager (state sub-state ui-element)
  `(progn (manager.init? ,state ,sub-state)
	  (setf (gethash
		 (ui-id ,ui-element) (ui-manager-collection
				      (gethash (aux:to-keyword ,sub-state) (gethash (aux:to-keyword ,state) ui-managers))))
		,ui-element)))

(defun add-state-ui-element (state sub-state ui-element &key in-container in-window in-frame)
  (manager.init? state sub-state)
  (if (or (not in-container) (not in-window) (not in-frame))
      (push (ui-id ui-element)
	    (ui-manager-collection-ids (gethash sub-state (gethash state ui-managers)))))
  (setf (gethash (ui-id ui-element) (ui-manager-collection (gethash sub-state (gethash state ui-managers))))
	ui-element)
  ;;  (ui.color-check (gethash (ui-id ui-element) (ui-manager-collection (gethash sub-state (gethash state ui-managers)))))
  )


(defmacro ui-element.make (state sub-state (type x y w h &key action custom label entry constructor color))
  `(let ((element nil)
	 (color ,color)
	 (constructor-string
	   (if ,constructor
	       (concatenate 'string "make-ui-" (remove #\: (write-to-string ,constructor)))
	       (concatenate 'string "make-ui-" (remove #\: (write-to-string ,type))))))
     (if (not (listp color))
	 (setf color (eval color)))
     (setf element (funcall (aux:read-string constructor-string)
			       :type ,type
			       :x ,x :x-init ',x
			       :y ,y :y-init ',y
			       :width ,w :width-init ',w
			       :height ,h :height-init ',h
			       :action ,action :custom ,custom
			       :label ,label :entry ,entry
			       :color color))
     (push (ui-id element) (ui-manager-collection-ids (get-ui-manager ,state ,sub-state)))
     (setf (gethash (ui-id element)  (ui-manager-collection (get-ui-manager ,state ,sub-state))) element)))


(defmacro ui.add-scroll-bar (state sub-state scroll-bar)
  `(ui.add-to-manager ,state ,sub-state ,scroll-bar))

(defmacro ui-manager.add-element (state sub-state ui-element)
  `(progn (manager.init? ,state ,sub-state)
	  (push (ui-id ,ui-element) (ui-manager-collection-ids (gethash ,sub-state (gethash ,state ui-managers))))
	  (setf (gethash (ui-id ,ui-element)
			 (ui-manager-collection (gethash ,sub-state
							 (gethash ,state ui-managers))))
		,ui-element)))

(setf (fdefinition 'ui.add-element) #'add-state-ui-element)


(defmacro ui-manager.define-element (state sub-state (type x y w h &key action special label entry))
  `(add-state-ui-element ,state ,sub-state (make-ui-element :type ,type
							  :x ,x :x-init ',x
							  :y ,y :y-init ',y
							  :width ,w :width-init ',w
							  :height ,h :height-init ',h
							  :action ,action :special ,special
							  :label ,label :entry ,entry)))


(defun add-state-ui-elements (state sub-state &rest ui-elements)
  (loop for ui-element in ui-elements
	do (add-state-ui-element state sub-state ui-element)))

(setf (fdefinition 'ui.add-elements) #'add-state-ui-elements)

(defun menu.set-width (menu)
  (let ((highest 0))
    (loop for id in (ui-menu-ids menu)
       do (setf id (write-to-string id))
	 (if (> (length id) highest)
	      (setf highest (length id))))
    (setf (ui-menu-width menu) (* (car tamias.string:character-size) highest))))

(defun menu.recurssive-width (menu)
  (menu.set-width menu)
  (if (ui-menu-active-item menu)
      (loop for id in (ui-menu-ids menu)
	    do (let ((menu-item (gethash id (ui-menu-children menu))))
		 (menu.recurssive-width menu-item)))))

(defun stringer (&rest elements)
  (let ((ret-str ""))
    (loop for obj in elements
	  do (if (not (stringp obj))
		 (setf ret-str (concatenate 'string ret-str (write-to-string obj) " "))
		 (setf ret-str (concatenate 'string ret-str obj " "))))
    ret-str))

(defun err-str (elements)
  (let ((ret-str ""))
    (loop for obj in elements
	  do (if (not (stringp obj))
		 (setf ret-str (concatenate 'string ret-str (write-to-string obj) " "))
		 (setf ret-str (concatenate 'string ret-str obj " "))))
    ret-str))


(defmacro ui-error-check (element &rest err-str)
  `(if (not (ui-p ,element))
       (error (err-str ',err-str))))	   
