;;Mouse handled in container code

(defmethod ui.kb-input (ui-element (ui-type (eql :window)) (kb-input (eql :down)))
  ;;move the scroll bar down
  )

(defmethod ui.kb-input (ui-element (ui-type (eql :window)) (kb-input (eql :up)))
  ;;move the scroll bar up
  )

(defmethod ui.kb-input (ui-element (ui-type (eql :window)) (kb-input (eql :enter)))
  )
