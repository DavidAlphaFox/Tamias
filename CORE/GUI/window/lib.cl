(defstruct (ui-window (:include ui-container
		       (type :window)
		       (children (make-hash-table))))
  state
  sub-state
  (view (make-array 4))
  scroll-h
   ;;(make-scroll-bar :horizontal? t))
  scroll-v)

(defmacro ui-window-view-x (ui-window) `(aref (ui-window-view ,ui-window) 0))

(defmacro ui-window-view-y (ui-window) `(aref (ui-window-view ,ui-window) 1))

(defmacro ui-window-view-width (ui-window) `(aref (ui-window-view ,ui-window) 2))

(defmacro ui-window-view-height (ui-window) `(aref (ui-window-view ,ui-window) 3))




(defmacro ui.add-window (state sub-state (&rest args
					  &key (pos-dim '(0 0 200 200))
					    (scroll-bar-color tamias.colors:+chalk-white+)
					    (window-color tamias.colors:+dark-pastel-grey+) &allow-other-keys)
			 &rest children)
  `(let ((scroll-h (make-scroll-bar :horizontal? t :color ',scroll-bar-color :height 16))
	 (scroll-v (make-scroll-bar :color ',scroll-bar-color :width 16))
	 (window (make-ui-window :x (elt ,pos-dim 0) :y (elt ,pos-dim 1)
				 :width (elt ,pos-dim 2) :height (elt ,pos-dim 3)
				 :color ',window-color)))
     (ui.add-scroll-bar ',state ',sub-state scroll-h)
     (ui.add-scroll-bar ',state ',sub-state scroll-v)

     (aux:push-to-end (ui-id scroll-h) (ui-ids window))
     (aux:push-to-end (ui-id scroll-v) (ui-ids window))
     
     (setf (gethash (ui-id scroll-h) (ui-children window))
	   scroll-h
	   (gethash (ui-id scroll-v) (ui-children window))
	   scroll-v)
     (if ',children
	 (loop for child in ',children
	       do (push (ui-id child) (ui-ids window))
		  (setf (gethash (ui-id child) (ui-children window))
			child)))
     (ui.add-element ',state ',sub-state window)))

