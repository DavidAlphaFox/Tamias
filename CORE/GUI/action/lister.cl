(defmethod ui.mouse.click (ui-element (ui-type (eql :lister)))
  (if (ui-lister-selector-active? ui-element)
        #|
Then a bunch of code that checks the offset and shit
  to figure out which object to set the selector to
|#
      "click")
  (setf (ui-lister-selector-active? ui-element) t))

(defun execute-lister-obj-callback (lister-obj)
  )

(defmethod ui.kb-input (ui-element (ui-type (eql :lister)) (kb-input (eql :down)))
  (selection-cursor-down (ui-lister-selector ui-element)
			 (1- (length (ui-lister-child-objects ui-element))) t)
  ;;(ui-lister-total-objs ui-element)
  )

(defmethod ui.kb-input (ui-element (ui-type (eql :lister)) (kb-input (eql :up)))
  (selection-cursor-up (ui-lister-selector ui-element)
		       (1- (length (ui-lister-child-objects ui-element))) t)
  )

(defmethod ui.kb-input (ui-element (ui-type (eql :lister)) (kb-input (eql :enter)))
  (let ((lister-obj (get-current-lister-object ui-element)))
    (if (lister-obj-callback lister-obj)
	(funcall (lister-obj-callback lister-obj)
		 lister-obj)
	(funcall (ui-lister-callback-sym ui-element)
		 lister-obj))))
