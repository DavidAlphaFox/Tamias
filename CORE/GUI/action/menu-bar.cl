(defmethod ui.mouse.click (ui-element (ui-type (eql :menu-bar)))
  (let ((x-acc 0)
	(x-point tamias:*mouse-x*))
    (loop for item in (ui-menu-bar-ids ui-element)
	  do (let ((str-size (* (car tamias.string:character-size)
				(length (tamias-string-text (ui-label (gethash item (ui-children ui-element))))))))
	       (print item)
	       (if (and (>= x-point x-acc)
			(<= x-point (+ x-acc str-size)))
		   (return (setf (ui-menu-bar-active-item ui-element) item
				 (ui-menu-item-active? (ui.menu-bar-child
							ui-element
							item))
				 t))
		   (incf x-acc (+ (ui-padding-x ui-element) str-size)))))))
  ;;Check mouse-x against each item, set it to the active item and then drop down/open right, unless the active item is clicked, then deactivate the active-item, aka set to 0
;;If the mouse-x and mouse-y are outside the region of the possible menu-bar-items, it deactivates the menu-bar and deactivates all the other things, and sets sub-state to 'top

(defmethod ui.mouse.click (ui-element (ui-type (eql :menu-item)))
  (if (ui-element-action ui-element)
      (eval (ui-element-action ui-element))))

(defun gui.menu-bar-item.click (ui-menu-bar-item)
  (let ((initial-x (ui-menu-item-x ui-menu-bar-item))
	(width-accumulator (ui-menu-item-width ui-menu-bar-item))
	)
    (declare (ignorable initial-x width-accumulator))
    ))
