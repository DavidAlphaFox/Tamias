#|

The flow

Note on mouse: create a boundary box for the mouse, i.e. from -3 to +3 of the mouse-x and mouse-y

Click in window -> tamias.gui.mouse-click -> check if menu-bar is active item? -> No? If item is located under click area, 

|#

;;ui.mouse.click assumes that the mouse-click is within the confines of the ui-element


;;(incf (tamias-variable-value (ui-spin-box-value (ui-element-special ui-element))))
;;(decf (tamias-variable-value (ui-spin-box-value (ui-element-special ui-element))))

;;;Forgot about this, but with the symbol, make it a spinbox with a max of 255. Use the integer to display an Ascii character

(defvar gui-key-pressed? nil)
(defvar gui-key-timer 0)

;;(defvar mouse.ui (vector tamias:*mouse-x* tamias:*mouse-y*))

(define-symbol-macro mouse.ui-x tamias:*mouse-x*)
(define-symbol-macro mouse.ui-y tamias:*mouse-y*)

(defun tamias.gui.key-press (key)
    ;;get the current active ui element
    ;;check if it has a hash entry for the current bit-mask
    ;;if there is, then gethash (elt tamias-current-key 0)
  ;;if non-nil, then execute the command in the hash-entry
  (when (ui.current-manager)
    (when (ui-p active-ui-element)
      (ignore-errors
       (when (not gui-key-pressed?)
	 (when (ui.kb-input active-ui-element (ui-type active-ui-element) key)
	   (setf gui-key-pressed? t)))))))

#|	(if (not (eq (ui-type active-ui-element) entry))
	    (let ((ui-ip (gethash (tamias:kb-key-masks) (ui-input active-ui-element))))
	      (if ui-ip
		  (eval (gethash (elt tamias-current-key 0) ui-ip))))))))
|#
  

(defun action.prep (ui-element ui-type)
  (if (ui-entry-p ui-element)
      (if (not (eq current-text-context (ui-entry ui-element)))
	  (deactivate-entry)))
  (ui.mouse.click ui-element ui-type))

#|
"Uh huh. So, this is still development code (great).
So gui.click.check goes through each non menu-bar element and determines what got clicked by the mouse (or pointer)
The gist is tamias.gui.click : -> menu-bar active? -> no -> is the pointer in the range of the menu-bar? -> no -> Then check which element was clicked

Ah HAH! click.check /already/ deals with activating elements. So, for example, it goes through a frames elements and if it did something, then it exits the loop
I've done a small improvement to it and added a case statement. 
If you're reading this, just remember: Documentation is important. For every second it takes to write a procedure, it requires 1 minute to decipher.
"
|#

(defun gui.click.check ()
  (let ((ui-manager (get-current-ui-manager))
	(clicked nil))
    ;;    (print "click")
    (loop for id in (remove 'menu-bar (ui-manager-collection-ids ui-manager))
	  do (let ((element (gethash id (ui-manager-collection ui-manager))))
	       (ui.set-active element)
	    (case (ui-type element)
	      ((:frame 
		:window 
		:dock 
		:container)
	       (gui.container.click element))
	      (otherwise (if (mouse.test-position
			      (ui-x element) (ui-y element)
			      (ui-width element) (ui-height element) 'box)
			     (return (progn (setf clicked t)
					    (action.prep element (ui-base-type element)))))))))
    (if (not clicked)
	(deactivate-entry))))

(defun gui.get-menu-item.click (parent-item &optional (x-offset 0) (y-offset 0)) ;;is a ui-menu-item
  (if (ui-ids parent-item)
      (loop for id in (ui-ids parent-item)
	    do (let ((child-item (gethash id (ui-children parent-item))))
;;		 (print id)
		 (let ((result (gui.get-menu-item.click child-item x-offset y-offset)))
#|		   (format t "child: x y w h ~A ~A ~A ~A"
			   (+ x-offset (ui-x child-item))
			   (+ y-offset (ui-y child-item))
			   (ui-width child-item)
			   (ui-height child-item))|#
		   (if result
		       (return t)
		       (incf y-offset 16))
		 )))
      (if (mouse.test-position (+ x-offset (ui-x parent-item))
			       (+ y-offset (ui-y parent-item))
			       (ui-width parent-item)
			       (ui-height parent-item)
			       'box)
	  (eval (ui-element-action parent-item)))))

(defun handle-menu-bar-input ( active-item-id ui-menu-bar )
  (let ((current-mb-item (gethash
			  active-item-id (ui-menu-bar-children ui-menu-bar))))
    (if (> mouse.ui-y (cadr tamias.string:character-size))
	(let ((evaled? (gui.get-menu-item.click current-mb-item
						(ui-x current-mb-item)
						(ui-height ui-menu-bar))))
	  (if evaled?
	      (setf (ui-menu-bar-active-item ui-menu-bar) nil)
	      (if (or (> mouse.ui-x (ui-menu-item-x current-mb-item))
		      (< mouse.ui-x (+ (ui-menu-item-x current-mb-item)
				       (ui-menu-item-width current-mb-item))))
		  (setf (ui-menu-bar-active-item ui-menu-bar) nil))))
	(if (or (< mouse.ui-x (ui-menu-item-x current-mb-item))
		(> mouse.ui-x (+ (ui-menu-item-x current-mb-item)
				 (ui-menu-item-width current-mb-item))))
	    (setf (ui-menu-bar-active-item ui-menu-bar) nil)))))

;;I should be able to reduce "handle-menu-bar-input" to ui.mouse.click :menu-bar right?
;;Isn't that the entire point of it?
;;
;;Oh my god. Oh god. I...this is what I was working on before 2/15/23...
;;This...oh
;;

(defun tamias.gui.click ()
  "Expand this function. Check if there is a menu-bar. current-menu-bar-item makes an assumption that is too dangerous for finalized code"
  (when (ui.current-manager)
    (let ((ui-menu-bar (get-current-menu-bar)))
      ;;check the mouse-timer somewhere here
      (if ui-menu-bar
	  (let ((active-item-id (ui-menu-bar-active-item ui-menu-bar)))
	    ;;check if the menu-bar of the gui-manager of the current state and sub-state has an active item, not '0.
	    (if active-item-id
		(handle-menu-bar-input active-item-id ui-menu-bar)
		;;then handle the mouse click, going through each entry in the ui-menu-bar's sub-menus and such. If the mouse-x lies outside the beginning or end of the start and finish of the sub-menus, then deactivate the menus. This is alpha code, so don't worry about making it perfect.
		;;if no menu-bar items are active, then go through ui-manager-collection and check it against each element until it gets a hit. This is why Frames are a good idea. If a mouse-click lies outside the frame, then it goes to the next frame, and so on.
		(if (> tamias:*mouse-y* (cadr tamias.string:character-size))
		    (gui.click.check)
		    ;;then go through each frame and check if the mouse click is on any of the underlying elements
		    (progn (deactivate-entry)
			   (ui.mouse.click ui-menu-bar :menu-bar)))))
	  (gui.click.check)))))
  
