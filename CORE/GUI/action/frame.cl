
THIS FILE IS NOT BEING USED

DO NOT INCLUDE THIS FILE

(defun gui.frame.click (element x-acc y-acc)
  (if (mouse.test-position (+ x-acc (ui-base-x element))
			   (+ y-acc (ui-base-y element))
			   (ui-base-width element)
			   (ui-base-height element)
			   'box)
   #|(and (> *mouse-x* (+ x-acc (ui-base-x element)))
	   (< *mouse-x* (+ x-acc (ui-base-x element) (ui-base-width element)))
	   (> *mouse-y* (+ y-acc (ui-base-y element)))
	   (< *mouse-y* (+ y-acc (ui-base-y element) (ui-base-height element))))|#
      (if (eq (ui-base-type element) 'frame)
	  (loop for id in (ui-frame-ids element)
	     do (let ((frame-element (gethash id (ui-frame-children element))))
		  (gui.frame.click frame-element (+ x-acc (ui-base-x element)) (+ y-acc (ui-base-y element)))))
	  (progn (setf mouse.ui (list (- tamias:*mouse-x* x-acc) (- tamias:*mouse-y* y-acc)))
		 (ui.mouse.click element (ui-base-type element))))))

"Go through each item in the window and see if the mouse clicked on a viable element"
(defun gui.window.click (element x-acc y-acc)
  (declare (ignorable element x-acc y-acc))
  )
