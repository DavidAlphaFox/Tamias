"I am a massive colonizer asshole, so I assume everybody reads left to right.
Because I'm American.

In all seriousness, I'll add in support for right-to-left in later versions.
I'm more worried about having a working program to begin with than supporting every edge case."

"Now then, explaining how this works lol"
(defun ui-pack-elements (container &rest elements)
  (flet ((next-el (el el-list) (1+ (position el) el-list)))
    (loop :for n :below (length elements)
	  do (loop for el in elements
		   do (setf (ui-x el) (ui-pack-offset-x container))
		      (incf (ui-pack-offset-x container) (ui-width el))
		      (if (or (>= (ui-pack-offset-x container) (ui-width container))
			      (>= (+ (ui-pack-offset-x container)
				     (ui-width (next-el el elements)))
				  (ui-width container)))
			  (setf (ui-pack-offset-x container) 0
				;;INCOMPLETE
				))))))

(defun ui-pack-group (container &rest elements)
  (loop for el in elements
	do (setf (ui-x el) (ui-pack-offset-x container))
	   (incf (ui-pack-offset-x container) (ui-width el))
	   (if (or (>= (ui-pack-offset-x container) (ui-width container))
		   (>= (+ (ui-pack-offset-x container)
			  (ui-width (next-el el elements)))
		       (ui-width container)))))
