;;:up and :down based on visual up and down
(defmacro quick-cycle (selection direction)
  (let ((down :down)
	 (up :up))
    `(if (or (eq ,direction 1)
	     (eq ,direction ,down))
	 (incf ,selection)
	 (if (or (eq ,direction -1)
		 (eq ,direction ,up))
	     (decf ,selection)))))

(defmacro selection-cursor-up (selection &optional max-selection reset)
  `(progn (if (< (1- ,selection) 0)
	      (if ,reset
		  (setf ,selection ,max-selection))
	      (quick-cycle ,selection :up))))

(defmacro selection-cursor-down (selection &optional max-selection reset)
  `(progn (if (>= ,selection (or ,max-selection (1+ ,selection)))
	      (if ,reset
		  (setf ,selection 0))
	      (quick-cycle ,selection :down))))

(defun remove-buffer-on-quit (ui-element)
  (let ((id (ui-id ui-element))
	(state (tamias:state-symbol tamias:state))
	(sub-state (tamias:state-sub-state tamias:state)))
    (eval `(push-quit (render:remove-buffer (tamias-string-buffer (ui-label (get-ui-element ,id ,state ,sub-state))))))))

(defmacro ui.destroy-el (state sub-state id)
  `(setf (ui-manager-ids (get-ui-manager ',state ',sub-state))
	 (remove ,id (ui-manager-ids (get-ui-manager ',state ',sub-state)))
	 (get-ui-element ,id ,state ,sub-state) nil))

(defmacro ui.make-label (x y label &key hidden width height
				     (use-buffer t) scale-text
				     text-color
				     color)
  `(let* ((w (or ,width (* (length ,label) (car tamias.string:character-size))))
	  (h (or ,height (cadr tamias.string:character-size))))
     (make-ui-label :x ,x :x-init ',x
		    :y ,y :y-init ',y
		    :width w :width-init (or ',width (list * (length ,label) (car tamias.string:character-size)))
		    :height h :height-init (or ',height '(cadr tamias.string:character-size))
		    :hidden ,hidden :use-buffer ,use-buffer
		    :scale-text ,scale-text :color ,color
		    :text-color ,text-color)))
  
  

(defmacro ui.add-label (state sub-state x y label &key hidden width height
						    (use-buffer t) scale-text
						    (text-color tamias.colors:+white+)
						    (color tamias.colors:+black+))
  `(let ((t-col ',text-color)
	 (ui-col ',color))
     (let ((ui-el (ui.make-label ,x ,y ,label
				 :hidden ,hidden :width ,width :height ,height
				 :use-buffer ,use-buffer :scale-text ,scale-text
				 :text-color t-col :color ui-col)))
       (setf (tamias-string-text (ui-label-label ui-el)) ,label)
       (ui-element.add ',state ',sub-state ui-el)
       )))

(defmacro ui.child-label (state sub-state x y label
			  &key width height
			    scale-text
			    use-buffer
			    (text-color tamias.colors:+white+)
			    (color (tamias.colors:offset tamias.colors:+black+)))
  `(let* ((t-col ',text-color)
	  (ui-col ',color)
	  (ui-el (ui.make-label ,x ,y ,label
				:width ,width :height ,height
				:scale-text ,scale-text
				:use-buffer ,use-buffer
				:text-color t-col :color ui-col)))
     (setf (tamias-string-text (ui-label ui-el)) ,label)
     (ui.add-to-manager ',state ',sub-state ui-el)
     ui-el))


(defmacro ui.add-spin-box (state sub-state (x y w h &key (color '(95 95 95 255)) action))
  `(ui-element.make ',state ',sub-state
       (:spin-box ,x ,y ,w ,h :action ,action :custom (make-number-entry) :label "0" :color ',color)))


#|

Example:
(get-menu-bar-sub-items (get-menu-bar-item '3d-editor 'sculpt 'file))
(get-menu-bar-sub-item 'new (get-menu-bar-item '3d-editor 'sculpt 'file))

|#
(defmacro ui.add-lister (state sub-state (x y w h &optional (bg-color (tamias.colors:offset tamias.colors:+black+)))
			 (&rest children-strings))
  `(let ((lister-objs nil)
	 (ui-lister (make-ui-lister :x ,x :x-init ',x
				    :y ,y :y-init ',y
				    
				    :width ,w :width-init ',w
				    :height ,h :height-init ',h)))
     (loop for child-obj in ',children-strings
	do (if (listp child-obj)
	       (if (length= child-obj 3)
		   (push (make-lister-obj
			  :str (car child-obj)
			  :type (cadr child-obj)
			  :callback (caddr child-obj)) lister-objs)
		   (push (make-lister-obj
			  :str (car child-obj)
			  :type (cadr child-obj)) lister-objs))
	       (push (make-lister-obj :str child-obj) lister-objs)))
     (setf (ui-lister-child-objects ui-lister) (reverse lister-objs)
	   (ui-lister-total-objs ui-lister) (length lister-objs))
     (ui-element.add ',state ',sub-state ui-lister)))

