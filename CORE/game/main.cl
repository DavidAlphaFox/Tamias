(define-state gui-tester t idle)
(set-state gui-tester idle)

(defvar rend-text nil)
(state.init-ui gui-tester idle)

(ui.add-label gui-tester idle 300 300 "Guys, It Works! This splits text and wraps it!"
	      :use-buffer nil :width 70 :height 134 :hidden `,'rend-text :color tamias.colors:+cobalt+)
;;(add-state-ui-element 'gui-tester 'idle (make-ui-label :x 300 :x-init 300 :y 300 :y-init 300 :width 70 :width-init 70 :height 134 :height-init 134 :label  :hidden `,'rend-text))
(add-state-ui-element :gui-tester :idle (make-ui-button :x 200 :x-init 200 :y 200 :y-init 200 :action `(setf rend-text (not rend-text)) :color (list 0 90 0 255)))
(add-state-ui-element :gui-tester :idle (make-ui-text :x 50 :x-init 50 :y 52 :y-init 20 :width 400 :width-init 400 :height 24 :height-init 24))

(ui.add-spin-box gui-tester idle (600 32 (* 16 4) 32))
;;(add-state-ui-element 'gui-tester 'idle (make-ui-spin-box :x 600 :y 32 :width (* 16 4) :height 32 :color '(127 127 0)))

(menu-bar.init gui-tester idle)

(menu-bar.add-item gui-tester idle file)
(menu-bar.item.add-item new file gui-tester idle)
(menu-bar.child.add-item open file gui-tester idle)
(menu-bar.item.add-item shaboopie file gui-tester idle)
(menu-bar.item.add-item exit file gui-tester idle :action (quit-game))

(menu-bar.add-item gui-tester idle Edit)
(menu-bar.add-item gui-tester idle view)

