(define-state sci-fi)
(set-state sci-fi 'idle)

(ui.add-lister sci-fi idle
    (8 16 200 (* char-height 7))
    ("Attack" "Tech" "Defend" "Item"
	      "Switch" "Set Combo!" "Run"))

(ui.add-lister sci-fi idle
    (400 16 200 (* char-height 7))
    ("Attack" "Runes" "Defend" "Item"
	      "Switch" "Tech" "Run"))
