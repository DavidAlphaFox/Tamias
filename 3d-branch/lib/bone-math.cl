#|

Time for some fun!

So, how to do this:
use calculate-transformation to get the transformation matrix per 'frame'
now, get the timing of that point in animation (i.e. 28.0, but KF[0] is 14.0 and KF[1] is 30.0)
normalize the time to '1', so [tA, tB] = [KF[0] - KF[0] + 1, KF[1] - KF[0] + 1], next normalize the timing to '1', so timing - tB - 1
next, multiply the transformation-per-frame matrix by the normalized timing, and voila, you have the transformation at t.

So:
trans-mat = (calc-trans bone[0 0] bone[1 0] kf[0] kf[1])
-> #2A((0.0 0.0 1.2e-12 0) (0.0 5.55555e-12 0.0 0.0) (2.1e-12 0.0 0.0 0.0) (0.0 0.0 0.0 0.0))
normalize: [1, kf[1] - kf[0] + 1] = [1.0, 5.0], current-time = 8.0, normalized ct = 3.0
ct * trans-mat
->#2A((0.0 0.0 3.6e-12 0) (0.0 1.555555e-11 0.0 0.0) (6.3e-12 0.0 0.0 0.0) (0.0 0.0 0.0 0.0))

My recommendation is to get the transformation mats of the bones into an array and send this array of mats to the GPU
Why calc on the CPU first? It's 32 bones once per frame vs 32 bones once per vertex. sure on 4 verts it isn't a big deal, but most models with armatures don't have just 4 verts
The timing should calced on the CPU, along with the matrices of that frame.
Once those calculations are done (the ones that would take up resources on the GPU) then you send them to the GPU, so that you aren't redoing the /exact same/ calculations
every vertex.
Also, the calculated transform should NOT be saved. You can, but I wouldn't recommend it.


I was misunderstanding some things about bone animations:
So, inverse bind pose is (1 / bind pose) not (-1 * bind pose) Me am dumb dumb
I am very dumb. the inverse of a matrix is not (1/mat[i j]). It's much more complicated than that. Gettin' the determinant and fuckin bullshit
Get the Determinant of a 4x4 matrix, then get the determinants of all the 2x2 matrices of the 4x4 matrix. 
This 4x4 matrix of the determinants of the 2x2 mats is called the adjugate matrix
Now, you take the adjugate matrix and divide each element by the determinant of the 4x4 matrix
Now you have the inverse of the original 4x4 matrix

Simple, right?



So: Frame 4 animation:
get the transformation at Frame 4 (matA keyframe0, matB keyframe1) -> kf0 = 1 kf1= 9 -> matA(1.0) matB(9.0)
matC=(8.0) next matc/num-frames, so matD=(1.0), next matD*(frame_num-kf0) = (3.0)


|#
(defun calculate-frame-transformation (mat-A mat-B timing-A timing-B)
  "This calculates a matrix along a linear path of a keyframed bone animation. I.e. mat-A mat-B are bone-mat[0] and bone-mat[1] where 0 and 1 are the keyframes.
timing-A and timing-B are the times of the keyframes, for example 7.0 and 28.0."
  (let ((res-mat (make-mat4))
	(timing 0.0))
    (setf timing (- (- timing-B timing-A) 1.0))
    (loop for i below 4
	  do (loop for j below 4
		   do (setf (aref res-mat i j) (/ (- (aref mat-B i j) (aref mat-A i j)) timing)))) ;;so mat-B - mat-A would get us the total transformation between the 2 keyframes. Div by timing gives us, given linear interpolation, the 'rate of change' matrix.
    res-mat))

#|
(defun calculate-transformation (matA matB timing) ;;the timing is to multiply the frame-transformation by
)
|#
