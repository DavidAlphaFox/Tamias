(defun test-help ()
  (format t
	  "

To redisplay the test suite help guide:
evaluate (test-help)

Hi there :) Welcome to the test suite for the Tamias Game Engine
Tests currently available:
gui-tester  :  called with (gui-tester)

"))

(test-help)
