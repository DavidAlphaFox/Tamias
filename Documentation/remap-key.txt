#|
(defmacro remap-kb-key-vars (tkiv-orig tkiv-target state sub-state)
    (let* ((shift1 (shift-flag (car keyconditions-1)))
	 (ctrl1 (ctrl-flag (cadr keyconditions-1)))
	 (alt1 (alt-flag (caddr keyconditions-1)))
	 (mod-state-key 0)
	 (key-func1 
	  (gethash orig (gethash key-state (get-state-keys state sub-state))))
	 (ctrl2 (ctrl-flag (cadr keyconditions-2)))
	 (alt2 (alt-flag (caddr keyconditions-2)))
	 (shift2 (shift-flag (car keyconditions-2)))
	 (mod-state-target 0)
	 (key-func2
	  (gethash target (gethash key-state (get-state-keys state sub-state)))))
    (if (find-if #'alpha-char-p (write-to-string orig))
	(if shift1
	    (incf mod-state-key #b1)))
    (if ctrl1
	(incf mod-state-key #b10))
    (if alt1
	(incf mod-state-key #b100))
    (if (find-if #'alpha-char-p (write-to-string target))
	(if shift2
	    (incf mod-state-target #b1)))
    (if ctrl2
	(incf mod-state-target #b10))
    (if alt2
	(incf mod-state-target #b100))
    (if (not (gethash orig (gethash key-state (get-state-keys state sub-state))))
	(setf (gethash orig (gethash key-state (get-state-keys state sub-state)))
	      (make-hash-table)))
    (if (not (gethash target (gethash key-state (get-state-keys state sub-state))))
	(setf (gethash target (gethash key-state (get-state-keys state sub-state)))
	      (make-hash-table)))
    (setf key-func1 (gethash mod-state-key (gethash orig (gethash key-state (get-state-keys state sub-state))))
	  key-func2 (gethash mod-state-target (gethash target (gethash key-state (get-state-keys state sub-state))))
	  (gethash mod-state-key (gethash orig (gethash key-state (get-state-keys state sub-state)))) key-func2
	  (gethash mod-state-target (gethash target (gethash key-state (get-state-keys state sub-state)))) key-func1)))
  
|#
