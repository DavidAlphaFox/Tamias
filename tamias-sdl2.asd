(defsystem :tamias-sdl2
  :author "Brandon Blundell | Neon Frost"
  :maintainer "Brandon Blundell | Neon Frost"
  :license "MIT"
  :version "0.98"
  :description "A game engine built and designed in Common LISP."
  :entry-point "cl-user::main"
  :depends-on (:sdl2
	       :sdl2-image
	       :sdl2-mixer
	       :sdl2-ttf
	       :tamias)
  :components ((:module "CORE"
		:serial t
		:components
		((:module "backends"
		:serial t
		:components
			((:module "sdl2"
			  :serial t
			  :components
				  ((:module "render"
				    :serial t
				    :components
					    ((:file "string" :type "cl")
					     (:file "library")
					     (:file "text" :type "cl")
					     (:file "sprite" :type "cl")
					     (:file "palette-lib" :type "cl")
					     ;;(:file "Menus")
					     ))
				   (:module "input"
				    :serial t
				    :components
					    ((:file "mouse" :type "cl")
					     (:file "keyboard" :type "cl")))
				   (:module "audio"
				    :serial t
				    :components
					    ((:file "music")
					     (:file "sound")
					     (:file "sdl2" :type "cl")))
				   (:file "font-loader" :type "cl")
				   (:file "init-main.sdl2" :type "cl")))))))))

  
