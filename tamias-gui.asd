(defsystem :tamias-gui
  :author "Brandon Blundell | Neon Frost"
  :maintainer "Brandon Blundell | Neon Frost"
  :license "MIT"
  :version "0.6"
  :description "The gui subsystem for tamias."
  :default-component-class cl-source-file.cl
  :entry-point "cl-user::main"
  :depends-on (:tamias)
  :components ((:module "CORE"
		:serial t
		:components
		((:module "GUI"
		  :serial t
		  :components
		  ((:file "gui-lib")
		   (:module "window"
		    :serial t
		    :components
			    ((:file "lib")
			     (:file "render")
			     (:file "action")))
		   (:file "gui-documentation")
		   (:file "lib-render")
		   (:file "gui-manager-lib")
		   (:file "menu-bar-lib")
		   (:file "gui-manager")
		   (:file "aux-lib")
		   (:file "frame-lib")
		   (:module "action"
		    :serial t
		    :components
			    ((:file "input-lib")
			     (:file "menu-bar")
			     (:file "entry")
			     (:file "spin-box")
			     (:file "button")
			     (:file "container")
			     (:file "hover")
			     (:file "lister")))
		   (:module "render"
		    :serial t
		    :components
			    ((:file "hover")
			     (:file "click")
			     (:file "string")
			     (:file "scroll-bar")
			     (:file "container")
			     (:file "label")
			     (:file "button")
			     (:file "menu")
			     (:file "menu-bar")
			     (:file "entry")
			     (:file "spin-box")
			     (:file "frame")
			     (:file "lister")))
		   (:module "file-selector"
		    :serial t
		    :components
			    ((:file "lib")
			     (:file "render")
			     (:file "action")))
		   ))))))
