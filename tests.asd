(defsystem :tests
  :author "Brandon Blundell | Neon Frost"
  :maintainer "Brandon Blundell | Neon Frost"
  :license "MIT"
  :version "0.98"
  :description "A game engine built and designed in Common LISP."
  :depends-on (:sdl2
	       :sdl2-image
	       :sdl2-mixer
	       :sdl2-ttf
	       :tamias)
  :components ((:module "tests"
		:serial t
		:components
		((:file "gui-tester" :type "cl")
		 (:file "test-text" :type "cl")))))		
