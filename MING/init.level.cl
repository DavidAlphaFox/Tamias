(define-track level-track "MING/assets/flight-for-your-life.ogg")
(define-sound enemy-fire "MING/assets/enemy-fire.wav")
(define-sound player-fire "MING/assets/player-fire.wav")
(define-sound boss-fire "MING/assets/boss-fire.ogg")

(defun load-sprite-sheets ()
  ;;type-a through type-d (boss) and player
  (when (sprite:check-sheet :player)
    (sprite:defsheet :player "MING/assets/player.png" 32)
    (sprite:defsheet :type-a "MING/assets/Type-A.png" 32)
    (sprite:defsheet :type-b "MING/assets/Type-B.png" 32)
    (sprite:defsheet :type-c "MING/assets/Type-C.png" '(32 48))
    (sprite:defsheet :type-d "MING/assets/Type-D.png" '(64 128))))

(states:add-logic
    (main-menu init)
    (load-sprite-sheets))

(states:add-logic
    (level init)
    (init-sounds) 
    (switch-track-to level-track))
