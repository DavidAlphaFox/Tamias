(defmacro kill-ship (ship)
  `(if (player-p ,ship)
       "player is kill :p"
       (progn (if (not (player-invincible player))
		  (decf (player-hp player)))
	      (setf (ship-state ,ship) :dead))))

(defun edge-of-screen (ship)
  (if (< (ship-speed-x ship) 0)
      (<= (ship-x ship) 0) ;;left edge
      (>= (+ (ship-x ship) (ship-width ship)) tamias:screen-width))) ;;right edge

(defun enemy-dead? (e) ;;not sure if in use
  (or (eq (ship-state e) :dead)
      (> (ship-y e) (+ tamias:screen-height 96))
      (< (ship-y e) -392)
      (> (ship-x e) (+ tamias:screen-width 256))
      (< (ship-x e) -256)))

(defgeneric ship-fire (ship)
  (:method (ship)
    (declare (ignore ship))
    nil))

(defgeneric kill-entity (ship)
  (:method (ship)
    (declare (ignore ship))
    nil))
;;attack-state for enemy's fire

(defmethod kill-entity ((ship enemy))
  (generate-explosions (ship-x ship) (ship-y ship) 16 (/ fps 2));do some explosions
  (setf (enemy-state ship) :dead)
  (incf (player-kills player) 1)
  (setf (ship-y ship) (+ tamias:screen-height 32)))
;; explosion at + entity-x (/ size-x entity 2) and + entity-y (/ size-y entity 2)

(defgeneric ship-move (ship)
  (:method (ship)
    (declare (ignore ship))
    nil))

(defgeneric render-ship (ship)
  (:method (ship)
    (declare (ignore ship))
    nil))

#|
TYPE A: Vectors: +y at a rate of 0.5, +/- x at a rate of 0.25 until edge of the screen then change 'sign'
TYPE B: Inverse of Type A, faster +/- x, slower +y
TYPE C: Slams downward 5 times after firing 3 lasers, follows player; HP = 5 hits
BOSS: Vectors: +/- x, follows player at 1/4, 1/2, 3/4, and 1.0 players speed at 1.0, 3/4, 1/2, 1/4 health, no Y vector; HP=50 hits
|#


#|
4 basic enemy types
:a -Diagonal - slow ("bounces" off edge of "screen"), shoots when 1/4 of screen is left, shoots down
:b -Side to Side - Fast (goes down by about 2 pixels per second)
:c -Slammer - Fires 3 times, slams to the left, right and at the player
:d -Boss (after surviving 5 waves) - Slow, side to side. Shoots 1 projectile every 3 seconds, projectile moves at about 64 pixels per second (From generation point to offscreen in 6 seconds)
|#
