(defmethod kill-entity ((ship player))
  (generate-explosions (player-x) (player-y) 4)
  (set-sub-state dead)
  ;;(setf state 'game-over)
  ;;(free-sheet (entity-sheet-surface e))
  )

(defmethod ship-fire ((ship player))
  (when (eq (player-state player) :firing)
    (when (fire-timer (ship-fire-timer ship))    
      (spawn-bullet ship))))

(defmethod entity-fire-missile ((ship player))
  "missile" ;;spawn a missile
  (setf (player-missile-fired? player) t))

(defmethod ship-move ((ship player))
  (when (player-invincible ship)
    (timer:increment (player-invincible-timer ship))
    (when (timer:end? (player-invincible-timer ship))
      (setf (player-invincible ship) nil
	    (timer:current (player-invincible-timer ship)) 0)))
  (when (not (edge-of-screen ship))
    (incf (player-x) (player-speed-x)))
  (when (edge-of-screen ship)
    (if (< (player-x) 32)
	(setf (player-x) 0)
	(setf (player-x) (- tamias:screen-width (player-width ship)))))
  (when (not (player-invincible ship))
    (when (bullet-collide? ship)
      (setf (player-invincible ship) t)))
  (when (eq (player-move-state player) :slowing)
    (if (< (player-speed-x) 0)
	(decf (player-speed-x) (floor (/ (player-speed-x) 10)))
	(decf (player-speed-x) (ceiling (/ (player-speed-x) 10))))
    (if (= (player-speed-x) 0)
	(setf (player-move-state player) :idle)
	(if (and (< (player-speed-x) 0)
		 (> (player-speed-x) (- (floor (/ (/ tamias:screen-width tamias:fps) 10)))))
	    (setf (player-move-state player) :idle
		  (player-speed-x) 0)
	    (if (and (> (player-speed-x) 0)
		     (< (player-speed-x) (floor (/ (/ tamias:screen-width tamias:fps) 10))))
		(setf (player-move-state player) :idle
		      (player-speed-x) 0)))))
  (when (<= (ship-hp ship) 0)
    ;;game over
    ))

(defvar player-cell-timer (timer:make :end 6 :reset t))
(setf (timer:end player-cell-timer) 8)
(defmethod render-ship ((ship player))
  ;;left = cell 5
  ;;left, slow down = cell 4
  ;;null = cell 3
  ;; right slow down = cell 2
  ;;right = cell 1
  (if (> (player-speed-x) 0)
      (progn (setf (sprite:sheet-current-cell (player-sprite-sheet player)) 1);;going right
	     (timer:reset-timer player-cell-timer))
      (if (< (player-speed-x) 0)
	  (progn (setf (sprite:sheet-current-cell (player-sprite-sheet player)) 5);;going left
		 (timer:reset-timer player-cell-timer))
	  (if (not (eq (sprite:sheet-current-cell (player-sprite-sheet player)) 3))
	      (progn (if (eq (sprite:sheet-current-cell (player-sprite-sheet player)) 5)
			 (setf (sprite:sheet-current-cell (player-sprite-sheet player)) 4)
			 (if (eq (sprite:sheet-current-cell (player-sprite-sheet player)) 1)
			     (setf (sprite:sheet-current-cell (player-sprite-sheet player)) 2)))
		     (timer:increment player-cell-timer)
		     (when (timer:end? player-cell-timer)
		       (setf (sprite:sheet-current-cell (player-sprite-sheet player)) 3));;just flyin
		     ))))
  (when (player-invincible ship)
    (when (eq (mod (timer:current (player-invincible-timer ship)) 2) 0)
      (render:sprite (player-sprite-sheet player) (player-x) (player-y))))
  (if (not (player-invincible ship))
      (render:sprite (player-sprite-sheet player) (player-x) (player-y))))
