#|
I'm wondering if enemy-groups should...be their own struct and not just a list...

Well, here's the thing: You shouldn't really be creating new types unless you need a fairly large amount of specialization on that 'type'

That's why my bounding boxes, which are really just arrays, are just arrays with a few accessor functions
|#

;;now...we can just...push an enemy-group...onto...the...enemies stack
;;Man, this is SO much fucking better

#|

(defstruct entity
  sheet
  sheet-surface
  x
  y
  (cell 0)
  cell-size
  hp
  atk
  def
  size
  fire-sound ;make-sample :path "/path/to/sound"
  bullets
  max-hp);;;;only used with boss, but in entity for portability
|#



(defmethod remove-group (type group)
  (let ((t-p (position type enemies)))
    #|
    #######################################
    Why t-p? Because when it does the setf of t-p of enemies to nil, and then proceeds to use (position type enemies)
    the code runs into a interesting little error. You see, (position type enemies) returns a number. If the (nth (position type enemies) enemies) is nil, then...
    It returns nil for the position
    It's like erasing milk from a grocery list and then trying to find where milk is on the grocery list
    You can't find it because it got erased, so you get absolutely confused about why you were asked to find milk on the list when it was erased
    A good way to avoid this in code that is not this, is to do (if (position item seq) &body) so that it checks to make sure that point is not nil
    I do believe that solution was put into my RPG engine, but I forgot about it, I guess. If not, I'll put it in there.
    #######################################
    |#
    (if t-p
	(progn (if (every #'enemy-is-killed group)
		   (progn (incf (player-groups player) 1)
			  (setf (nth (position group (nth t-p enemies))
				     (nth t-p enemies)
				     ) nil)
			  (remove-nil (nth t-p enemies))
			  (remove-nil group))
		   (if (every #'enemy-dead group)
		       (progn (setf (nth (position group (nth t-p enemies))
					 (nth t-p enemies)
					 ) nil)
			      (remove-nil (nth t-p enemies))
			      (remove-nil group))
		       )))))
  (remove-nil enemies)
  ) ;loops through the enemies list and checks if a sub-list is all killed
x
