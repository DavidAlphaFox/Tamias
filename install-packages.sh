#!/bin/sh
#This is everything you should need to run Tamias
#
#Assumes no sbcl installation
#
#Assumes linux distributions with APT installed
sudo apt install curl gpg sbcl libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libsdl2-mixer-dev
cd ~
curl -O https://beta.quicklisp.org/quicklisp.lisp
curl -O https://beta.quicklisp.org/quicklisp.lisp.asc
gpg --verify quicklisp.lisp.asc quicklisp.lisp
echo "(quicklisp-quickstart:install) (ql:add-to-init-file) (exit)" >> tmp-ql-install.cl
sbcl --load "quicklisp.lisp" --load tmp-ql-install.cl
rm tmp-ql-install.cl
