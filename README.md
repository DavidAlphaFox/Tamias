# Tamias
A game engine implemented in Common LISP, by someone with Executive Dysfunction.

Focusing currently on 2D support, but 3D is planned down the road

As of right now, there is only support for the ogg music format. Use of mp3, wav or mod for _music_ is untested.

While I would like to implement my own audio server thing for Tamias, that will require a number of things. If I get around to properly developing Oreortyx, then it would be implemented in Rust or Zig, just because it would need to have as few things between it and the actual machine (and I don't want to do it in C)

# Building

- Install an implementation of common lisp, SBCL is the only one I can guarantee Tamias will work on.

- Install quicklisp.

- Install the developer and user libraries of sdl2, sdl2-image, sdl2-ttf and sdl2-mixer.

- From an SBCL instance that is in the working directory of your tamias folder (use (uiop:getcwd) to make sure you're in the correct directory, which should end with "/tamias" or "/tamias/"), type (load "init.cl") and then hit enter.

- After all of the various bits of information that pop up (style warnings and what not), type in (main) and hit enter. An sdl2 window should pop up with a black box that says "Console started" in it. [May 2023: (main) now starts a new thread for the sdl2 window. REPL can be used more easily for interactive development (especially with multi-monitor setups)]

- The exit key is set to C-` [control-backquote].

The first 'official' release is TBD

# Feedback

While I will appreciate feedback, please avoid being rude unless there is a piece of code that is completely braindead. I am trying to get rid of \`\,\'\, in my codebase

# Style

I do NOT always use the standard stylization practices by other lisp developers.

This is primarily due to the fact that Tamias is currently still in the development phase. I do not have a style guide. However, I do try to stick to some conventions, like +constant+. I will develop a style guide, not for further development of Tamias code, but to understand the various parts that don't make total sense.

An example of non-standard style: pseudo-package.fun-or-macro. Another example is the tamias.string package. This is to clarify that the "string" package isn't meant to override the default string functions, and that it more than likely relies on Tamias specific code (like tamias:state). The pseudo-package.fun-or-macro is used with the ui subsystem. I'm hoping that I can delineate the UI subsystem from tamias a bit, so that it can be used with other engines or toolkits

Recent development: I'll be using structure.function to differentiate from struct-fucntion to say "this is a user defined function." so ui-fs.get-file isn't part of the ui-fs defstruct, but it is integral to the use of ui-fs.

One thing to note about the pckg.fun-mac style is that it is never used with variables, ONLY functions or macros. I'll probably end up modifying the style to differentiate between functions and macros.

Another note: One liner macros (see CORE/engine/init/timers.cl) are next to each other because they aren't overly hard to read. At least, for me they aren't. If they are for other devs, then please, speak up.

# Updates
11/11/2021:
    Delineating input from state/sub-state system, input is tiered: key check :: mode -> player -> object -> state/sub-state
    Further work done on 3d system. Still far from production ready.
    UI implemented, many more elements still need to be added.
    Colors in own package. tamias.colors:+COLOR+ to use color
    Text subsystem is up and running, including fake anti-aliasing, may change font image to be anti-aliased at the source
    
    
12/20/2018:
    Added some animation handling code. will need to be tested more thoroughly.
    Keyboard should have been fixed to use virtual keys (:1) instead of physical keys (:scancode-1)
    Audio updated to help facilitate better looping
    Hex color string parser implemented, needs fine tuning
    
    Possibility of crashing: medium, I still need to work on the API documentation
	    
